<?php

class Frase extends Eloquent
{

    protected $table = 'home';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'frase' => 'required',
    ];

}
