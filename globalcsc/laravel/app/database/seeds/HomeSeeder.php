<?php

class HomeSeeder extends Seeder {

    public function run()
    {
        DB::table('home')->delete();

        $data = array(
            array(
                'frase' => 'Ajudar as organizações na conquista e superação de suas metas através da contratação dos melhores profissionais, gestão e retenção de talentos é a proposta da 5A.'
            )
        );

        DB::table('home')->insert($data);
    }

}