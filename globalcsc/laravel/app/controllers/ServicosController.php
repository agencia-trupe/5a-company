<?php

use \Servicos;

class ServicosController extends BaseController {

    public function index($pagina = null)
    {
        $servicos = Servicos::select(['pagina', 'titulo'])->ordenados()->get();

        $pagina = ($pagina ? Servicos::pagina($pagina)->first() : Servicos::ordenados()->first());
        if (!$pagina) App::abort('404');

        return $this->view('frontend.servicos', compact('servicos', 'pagina'));
    }

}
