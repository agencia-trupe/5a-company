@section('content')

    <legend>
        <h2><small>Institucional /</small> {{ $institucional->titulo }}</h2>
    </legend>

    {{ Form::model($institucional, [
        'route' => ['painel.institucional.update', $institucional->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.institucional._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop