@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem', 'Imagem')) }}
@if($submitText == 'Alterar' && $servicos->imagem)
    <img src="{{ url('../assets/img/servicos/'.$servicos->imagem) }}" style="display:block; margin-bottom: 10px; max-width:450px;height:auto;">
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.servicos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
