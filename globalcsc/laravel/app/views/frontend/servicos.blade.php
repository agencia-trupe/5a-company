@section('content')

    <main id="servicos" class="center">
        <div class="cabecalho">
            <h2>Serviços</h2>
            <img src="{{ asset('../assets/img/cabecalhos/'.$cabecalho->servicos) }}" alt="">
        </div>

        <div class="content">
            <nav>
                @foreach($servicos as $menu)
                <a href="{{ route('servicos', $menu->pagina) }}"@if($pagina->pagina == $menu->pagina) class='active'@endif>{{ $menu->titulo }}</a>
                @endforeach
            </nav>

            <div class="texto">
                <h2>{{ $pagina->titulo }}</h2>
                <div class="texto">
                    {{ $pagina->texto }}
                </div>
                @if($pagina->imagem)
                <div class="imagem">
                    <img src="{{ asset('../assets/img/servicos/'.$pagina->imagem) }}" alt="">
                </div>
                @endif
            </div>
        </div>
    </main>
@stop
