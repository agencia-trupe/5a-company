-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17-Abr-2015 às 16:55
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `5a-globalcsc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(2, '20150415124935_shutterstock_17150008.jpg', -1, '2015-04-15 15:49:38', '2015-04-15 15:49:38'),
(3, '20150415124942_shutterstock_57829741.jpg', -1, '2015-04-15 15:49:47', '2015-04-15 15:49:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cabecalhos`
--

CREATE TABLE IF NOT EXISTS `cabecalhos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institucional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servicos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cabecalhos`
--

INSERT INTO `cabecalhos` (`id`, `institucional`, `diferenciais`, `servicos`, `contato`, `created_at`, `updated_at`) VALUES
(1, '20150417145452_shutterstock_46846570.jpg', '20150417144619_Puzzle-piece.jpg', '20150417144619_shutterstock_50890975.jpg', '20150417144620_20150417133433_shutterstock_3389291.jpg', '0000-00-00 00:00:00', '2015-04-17 17:54:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE IF NOT EXISTS `chamadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `link`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '/', 'Administrativo', '20150415125602_shutterstock_86768056.jpg', -1, '0000-00-00 00:00:00', '2015-04-15 15:56:06'),
(2, '/', 'Financeiro', '20150415125638_shutterstock_61226158.jpg', -1, '0000-00-00 00:00:00', '2015-04-15 15:56:40'),
(3, '/', 'Tecnologia da Informação', '20150415125620_shutterstock_53322925.jpg', -1, '0000-00-00 00:00:00', '2015-04-15 15:56:21'),
(4, '/', 'Diferenciais', '20150415125626_shutterstock_64849642.jpg', -1, '0000-00-00 00:00:00', '2015-04-15 15:56:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `googlemaps`, `facebook`, `twitter`, `created_at`, `updated_at`) VALUES
(1, 'contato@5a.com.br', '11 2344-4596', '<p>S&atilde;o Paulo</p>\r\n\r\n<p>Rua Arizona, 1426 - 10&ordm; andar</p>\r\n\r\n<p>Brooklin - 04567-003</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.835174729641!2d-46.694737700000005!3d-23.610243499999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cc77c3e12d%3A0x84764471e584ebc!2sR.+Arizona%2C+1426+-+Cidade+Mon%C3%A7%C3%B5es%2C+S%C3%A3o+Paulo+-+SP%2C+04567-003!5e0!3m2!1spt-BR!2sbr!4v1427122411829" width="100%" height="100%" frameborder="0" style="border:0"></iframe>', 'http://www.facebook.com', 'http://www.twitter.com', '0000-00-00 00:00:00', '2015-04-13 16:20:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `diferenciais`
--

CREATE TABLE IF NOT EXISTS `diferenciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `diferenciais`
--

INSERT INTO `diferenciais` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>Ser dirigida pela competitividade do mercado, sendo seus servi&ccedil;os equivalentes aos produtos de uma empresa de servi&ccedil;os.</p>\r\n\r\n<p>Incorporar a vis&atilde;o de uma empresa semiaut&ocirc;noma, com ideologia organizacional e diretrizes estrat&eacute;gicas espec&iacute;ficas.</p>\r\n\r\n<p>Estar orientada a processos e focalizar atividades espec&iacute;ficas dentro dos processos.</p>\r\n\r\n<p>Alavancar os investimentos tecnol&oacute;gicos e perseguir a melhoria cont&iacute;nua.</p>\r\n\r\n<p>Estabelecer orienta&ccedil;&atilde;o de parceria com as demais unidades de neg&oacute;cio com as quais se relaciona, indo al&eacute;m da no&ccedil;&atilde;o tradicional de servi&ccedil;o ou suporte ao cliente.</p>\r\n', '', '0000-00-00 00:00:00', '2015-04-13 16:19:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `frase`, `created_at`, `updated_at`) VALUES
(1, 'A GLOBALCSC leva ao mercado uma forma de reduzir os processos operacionais corporativos e transacionais que não fazem parte do “core business” das organizações.', '0000-00-00 00:00:00', '2015-04-17 17:28:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `institucional`
--

CREATE TABLE IF NOT EXISTS `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `institucional`
--

INSERT INTO `institucional` (`id`, `pagina`, `titulo`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'descritivo', 'Descritivo', '<p>A GLOBALCSC &eacute; uma empresa do Grupo 5A Company que leva ao mercado uma forma de enxugar as processos operacionais corporativos e transacionais que n&atilde;o &nbsp;fazem parte do &ldquo;core business&rdquo; das organiza&ccedil;&otilde;es.</p>\r\n\r\n<p>Com o avan&ccedil;o da tecnologia criamos estruturas de fornecimento de servi&ccedil;os cada vez mais inovadores permitindo oferecer maior competitividade e foco aos nossos clientes.</p>\r\n\r\n<p>Dessa forma, atuamos na redu&ccedil;&atilde;o de custos, melhoria de produtividade liberando as &aacute;reas fins dos nossos clientes focarem em seus neg&oacute;cios sem a preocupa&ccedil;&atilde;o com as atividades transacionais.</p>\r\n', '', '0000-00-00 00:00:00', '2015-04-13 16:17:41'),
(2, 'missao', 'Missão', '<p>Ser uma empresa com excel&ecirc;ncia na presta&ccedil;&atilde;o de servi&ccedil;os otimizando e automatizando os processos de suporte ao neg&oacute;cio permitindo ganho de escala e redu&ccedil;&atilde;o de custos aos nossos clientes, garantindo a nossos colaboradores desafios e resultados aos investidores.</p>\r\n', '20150417144912_shutterstock_92360926.jpg', '0000-00-00 00:00:00', '2015-04-17 17:49:15'),
(3, 'valores', 'Valores', '<ul>\r\n	<li>Qualidade</li>\r\n	<li>Compet&ecirc;ncia</li>\r\n	<li>Agilidade</li>\r\n	<li>Comprometimento</li>\r\n</ul>\r\n', '20150417145118_shutterstock_20638894.jpg', '0000-00-00 00:00:00', '2015-04-17 17:51:20'),
(4, 'origem-do-nome', 'Origem do nome', '<p>A implanta&ccedil;&atilde;o dos servi&ccedil;os compartilhados ocupa lugar de destaque na atual estrat&eacute;gia de muitas empresas a luz de tr&ecirc;s conceitos disseminados no ambiente empresarial: economia de custos, redu&ccedil;&atilde;o da cadeia de valor e economia de escala.</p>\r\n\r\n<p>Em uma vis&atilde;o macro, o CSC atende a todas as &aacute;reas de uma empresa ou seja com a implementa&ccedil;&atilde;o do CSC teremos um atendimento GLOBAL. Com base nessa premissa nasceu o nome GLOBALCSC.</p>\r\n', '20150413132142_origem-nome.png', '0000-00-00 00:00:00', '2015-04-13 16:21:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_23_121033_create_chamadas_table', 1),
('2015_03_23_121057_create_banners_table', 1),
('2015_03_23_121212_create_home_table', 1),
('2015_03_23_121325_create_institucional_table', 1),
('2015_03_23_121342_create_diferenciais_table', 1),
('2015_03_23_121410_create_servicos_table', 1),
('2015_03_23_121419_create_contato_table', 1),
('2015_03_23_121434_create_contatos_recebidos_table', 1),
('2015_03_23_151503_create_cabecalhos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `servicos_pagina_unique` (`pagina`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `pagina`, `titulo`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'administrativo', 'Administrativo', '<p><strong>Suprimentos</strong></p>\r\n\r\n<ul>\r\n	<li>Compras</li>\r\n	<li>Recebimento</li>\r\n</ul>\r\n\r\n<p><strong>Servi&ccedil;os Internos</strong></p>\r\n\r\n<ul>\r\n	<li>Recep&ccedil;&atilde;o</li>\r\n	<li>Copa</li>\r\n</ul>\r\n\r\n<p><strong>Administra&ccedil;&atilde;o de Contratos</strong></p>\r\n\r\n<ul>\r\n	<li>Fornecedores</li>\r\n	<li>Prestadores</li>\r\n	<li>Cliente</li>\r\n	<li>Jur&iacute;dico</li>\r\n</ul>\r\n', '0', -1, '2015-04-13 16:23:29', '2015-04-13 16:26:15'),
(2, 'financeiro', 'Financeiro', '<ul>\r\n	<li>Faturamento</li>\r\n	<li>Cobran&ccedil;a</li>\r\n	<li>Contas a Receber</li>\r\n	<li>Contas a Pagar</li>\r\n	<li>Folha de pagamento</li>\r\n	<li>Tesouraria</li>\r\n	<li>Concilia&ccedil;&atilde;o</li>\r\n	<li>Administra&ccedil;&atilde;o de Fluxo de Caixa</li>\r\n	<li>Gera&ccedil;&atilde;o de Relat&oacute;rios Gerenciais</li>\r\n	<li>Cont&aacute;bil e Fiscal</li>\r\n</ul>\r\n', '0', -1, '2015-04-13 16:25:12', '2015-04-13 16:25:12'),
(3, 'tecnologia-da-informacao', 'Tecnologia da Informação', '<p><strong>HelpDesk</strong></p>\r\n\r\n<ul>\r\n	<li>Atendimento 1&ordm; N&iacute;vel</li>\r\n	<li>Direcionamento para as camadas de suporte ou gerenciamento</li>\r\n</ul>\r\n\r\n<p><strong>Suporte</strong></p>\r\n\r\n<ul>\r\n	<li>Opera&ccedil;&atilde;o de Computadores e Equipamentos Perif&eacute;ricos</li>\r\n	<li>Internet</li>\r\n	<li>Rede e Ativos de rede (Cabos, switches e roteadores)</li>\r\n	<li>Monitora&ccedil;&atilde;o de Ambientes, Servidores e Redes de Dados</li>\r\n	<li>Servidor de Proxy, Servidor de Arquivos e Servidor de Identidade (Active Directory)</li>\r\n	<li>Telefonia</li>\r\n	<li>Impressoras</li>\r\n	<li>Micro computadores e Notebooks</li>\r\n	<li>Softwares diversos</li>\r\n</ul>\r\n\r\n<p><strong>Gerenciamento</strong></p>\r\n\r\n<ul>\r\n	<li>Ambiente de Servidores</li>\r\n	<li>Ambiente de Rede e Telecomunica&ccedil;&otilde;es</li>\r\n</ul>\r\n', '0', -1, '2015-04-13 16:25:56', '2015-04-13 16:25:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$TXINOwfYmZHw/qSBb/Lx7.7qu/7w/RDZgFNOXey.5IVd1wUjv9gI6', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
