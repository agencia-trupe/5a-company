(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.slideshow = function() {
        var wrapper = $('.banners-wrapper');
        if (!wrapper.length) return;

        var itemInterval  = 3500,
            fadeTime      = 1000,
            numberOfItems = $('.banner').length,
            currentItem   = 0;

        $('.banner').eq(currentItem).show();

        if ($('.banner').length > 1) {
            var infiniteLoop = setInterval(function() {
                $('.banner').eq(currentItem).fadeOut(fadeTime);

                if (currentItem == numberOfItems - 1) {
                    currentItem = 0;
                } else {
                    currentItem++;
                }
                $('.banner').eq(currentItem).fadeIn(fadeTime);

            }, itemInterval);
        }
    };

    App.resizeChamadas = function() {
        var wrapper = $('.chamadas');
        if (!wrapper.length) return;

        var sameHeight = wrapper.height() - 80;
        wrapper.find('p').outerHeight(sameHeight);
    };

    App.contatoSubmit = function(event) {
        event.preventDefault();

        var $formContato = $(this);
        var $formContatoSubmit = $formContato.find('input[type=submit]');
        var $formContatoResposta = $formContato.find('#form-resposta');

        $formContatoResposta.hide();

        $.post(BASE + '/contato', {

            nome     : $('#nome').val(),
            email    : $('#email').val(),
            telefone : $('#telefone').val(),
            mensagem : $('#mensagem').val()

        }, function(data) {

            if (data.status == 'success') $formContato[0].reset();
            $formContatoResposta.hide().text(data.message).fadeIn('slow');

        }, 'json');
    };

    App.init = function() {
        App.slideshow();
        App.resizeChamadas();
        $('#form-contato').on('submit', App.contatoSubmit);
    };

    $(document).ready(App.init);

}(window, document, jQuery));
