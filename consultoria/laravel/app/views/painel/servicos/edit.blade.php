@section('content')

    <legend>
        <h2><small>Serviços /</small> {{ $servicos->titulo }}</h2>
    </legend>

    {{ Form::model($servicos, [
        'route' => ['painel.servicos.update', $servicos->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.servicos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop