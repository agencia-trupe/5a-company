@section('content')

    <main id="diferenciais" class="center">
        <div class="cabecalho">
            <h2>Diferenciais</h2>
            <img src="{{ asset('../assets/img/cabecalhos/'.$cabecalho->diferenciais) }}" alt="">
        </div>

        <div class="content">
            {{ $diferenciais->texto }}

            @if($diferenciais->imagem)
            <div class="imagem">
                <img src="{{ asset('../assets/img/diferenciais/'.$diferenciais->imagem) }}" alt="">
            </div>
            @endif
        </div>
    </main>
@stop
