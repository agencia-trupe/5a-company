<?php

class InstitucionalSeeder extends Seeder {

    public function run()
    {
        DB::table('institucional')->delete();

        $data = array(
            array(
                'pagina' => 'descritivo',
                'titulo' => 'Descritivo',
                'texto' => '...',
                'imagem' => 'img.jpg'
            ),
            array(
                'pagina' => 'missao',
                'titulo' => 'Missão',
                'texto' => '...',
                'imagem' => 'img.jpg'
            ),
            array(
                'pagina' => 'valores',
                'titulo' => 'Valores',
                'texto' => '...',
                'imagem' => 'img.jpg'
            ),
            array(
                'pagina' => 'origem-do-nome',
                'titulo' => 'Origem do nome',
                'texto' => '...',
                'imagem' => 'img.jpg'
            )
        );

        DB::table('institucional')->insert($data);
    }

}
