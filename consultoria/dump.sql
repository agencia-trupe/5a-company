-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17-Abr-2015 às 16:19
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `5a-consultoria`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(2, '20150417140422_Sales Training 2.jpg', 0, '2015-04-15 15:39:18', '2015-04-17 17:04:23'),
(3, '20150417140148_shutterstock_45239809.jpg', 1, '2015-04-15 15:39:35', '2015-04-17 17:01:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cabecalhos`
--

CREATE TABLE IF NOT EXISTS `cabecalhos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institucional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servicos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cabecalhos`
--

INSERT INTO `cabecalhos` (`id`, `institucional`, `diferenciais`, `servicos`, `contato`, `created_at`, `updated_at`) VALUES
(1, '20150417141114_Financial Results (5287551)jpg.jpg', '20150417141114_Puzzle-piece.jpg', '20150415123906_shutterstock_45216925.jpg', '20150417141114_20150417133433_shutterstock_3389291.jpg', '0000-00-00 00:00:00', '2015-04-17 17:11:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE IF NOT EXISTS `chamadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `link`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '/', 'Gestão Estratégica', '20150415124126_shutterstock_60027382.jpg', 0, '0000-00-00 00:00:00', '2015-04-17 16:53:53'),
(2, '/', 'Gestão de Processos', '20150415124139_shutterstock_56491816 (2).jpg', 1, '0000-00-00 00:00:00', '2015-04-17 16:54:03'),
(3, '/', 'Gestão de Projetos', '20150415124147_shutterstock_57740737.jpg', 2, '0000-00-00 00:00:00', '2015-04-17 16:54:11'),
(4, '/', 'Treinamento', '20150415124118_shutterstock_64782778.jpg', 3, '0000-00-00 00:00:00', '2015-04-15 15:41:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `googlemaps`, `facebook`, `twitter`, `created_at`, `updated_at`) VALUES
(1, 'contato@5a.com.br', '11 2344-4596', '<p>Alphaville</p>\r\n\r\n<p>Alameda Araguaia,&nbsp;2.190 -&nbsp;Torre I -&nbsp;3&ordm; Andar</p>\r\n\r\n<p>Barueri -&nbsp;06455-000</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.877678294674!2d-46.84094360000001!3d-23.500914999999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf021eb8ca5113%3A0x951f830123ad2e0f!2sAlameda+Araguaia%2C+2190+-+Alphaville+Industrial%2C+Barueri+-+SP!5e0!3m2!1spt-BR!2sbr!4v1429279024351" width="100%" height="100%" frameborder="0" style="border:0"></iframe>', 'http://www.facebook.com', 'http://www.twitter.com', '0000-00-00 00:00:00', '2015-04-17 16:57:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `diferenciais`
--

CREATE TABLE IF NOT EXISTS `diferenciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `diferenciais`
--

INSERT INTO `diferenciais` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>Criamos um conjunto de metodologias e tecnologias que aplicadas, propiciam conex&atilde;o entre produtos e servi&ccedil;os de modo consistente em todas as camadas da organiza&ccedil;&atilde;o.</p>\r\n\r\n<p>Esse conjunto de metodologias chamado Sirius considera a organiza&ccedil;&atilde;o em 3 camadas Estrat&eacute;gica, Operacional e de Aplica&ccedil;&atilde;o e propicia flexibilidade e liberdade no alinhamento dos objetivos organizacionais de forma sustent&aacute;vel e rent&aacute;vel.</p>\r\n\r\n<p>A estrat&eacute;gia do modelo &eacute; atuar da estrat&eacute;gia a opera&ccedil;&atilde;o conectando esfor&ccedil;os e multiplicando a disponibilidade de fatores para a obten&ccedil;&atilde;o de vantagem competitiva.</p>\r\n\r\n<h3><strong>MODELO SIRIUS</strong></h3>\r\n', '20150410174810_sirius.jpg', '0000-00-00 00:00:00', '2015-04-10 20:48:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `frase`, `created_at`, `updated_at`) VALUES
(1, 'A 5A Consultoria contribui significativamente no processo de transformação empresarial por meio da integração de pessoas, processos e ferramentas.', '0000-00-00 00:00:00', '2015-04-15 15:31:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `institucional`
--

CREATE TABLE IF NOT EXISTS `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `institucional`
--

INSERT INTO `institucional` (`id`, `pagina`, `titulo`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'descritivo', 'Descritivo', '<p>A 5A Consultoria empresa do grupo 5A Company contribui significativamente no processo de transforma&ccedil;&atilde;o empresarial por meio da integra&ccedil;&atilde;o de pessoas, processos e ferramentas.</p>\r\n\r\n<p>De origem brasileira, fundada em 2003, &eacute; reconhecida como uma das principais integradoras de solu&ccedil;&otilde;es do pais.</p>\r\n\r\n<p>Atua com foco em diversos segmentos de mercado, propiciando a consistente adequa&ccedil;&atilde;o a cada modelo de neg&oacute;cio, valorizando os talentos humanos e&nbsp;os reconhecendo como diferencial competitivo frente a atual velocidade imposta pelo mundo corporativo.</p>\r\n', '', '0000-00-00 00:00:00', '2015-04-17 16:52:05'),
(2, 'missao', 'Missão', '<p>Contribuir com a transforma&ccedil;&atilde;o, gest&atilde;o e opera&ccedil;&atilde;o dos nossos clientes por meio de Pessoas, Processos e Ferramentas.</p>\r\n', '20150417141903_future_gazing.jpg', '0000-00-00 00:00:00', '2015-04-17 17:19:05'),
(3, 'valores', 'Valores', '<ul>\r\n	<li>Conhecimento</li>\r\n	<li>Abundancia</li>\r\n	<li>Excel&ecirc;ncia</li>\r\n	<li>Transpar&ecirc;ncia</li>\r\n	<li>Verdade</li>\r\n	<li>Ousadia</li>\r\n</ul>\r\n', '20150417141630_business-success.jpg', '0000-00-00 00:00:00', '2015-04-17 17:16:30'),
(4, 'origem-do-nome', 'Origem do nome', '<p>Para que uma empresa suportada por equipes alcance os resultados esperados, &eacute; necess&aacute;rio que o Talento Humano (ser humano) esteja orientado a manter a <strong>Aten&ccedil;&atilde;o </strong>no que faz e no que acontece ao seu redor, identificando as oportunidades e os obst&aacute;culos, dessa forma ter&aacute; possibilidade de criar <strong>Alternativas </strong>que aproveitem as oportunidades e superem os obst&aacute;culos. Uma Alternativa deve sempre ser acompanhada de uma boa dose de <strong>Atitude</strong>, permitindo assim a sua realiza&ccedil;&atilde;o, por sua vez, as Alternativas devem buscar um n&iacute;vel de <strong>Assertividade</strong>. Por&eacute;m de nada adiantar&aacute; a Aten&ccedil;&atilde;o, a gera&ccedil;&atilde;o de Alternativas, a Atitude e a Assertividade se o talento humano n&atilde;o <strong>Acreditar </strong>no que se prop&otilde;e a fazer na vida profissional e pessoal.</p>\r\n\r\n<p>Desta forma, estes 5 elementos que diferenciam um profissional de outro, s&atilde;o os elementos fundamentais que fazem parte do pr&oacute;prio nome da empresa.</p>\r\n', '20150410173605_origem.jpg', '0000-00-00 00:00:00', '2015-04-10 20:36:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_23_121033_create_chamadas_table', 1),
('2015_03_23_121057_create_banners_table', 1),
('2015_03_23_121212_create_home_table', 1),
('2015_03_23_121325_create_institucional_table', 1),
('2015_03_23_121342_create_diferenciais_table', 1),
('2015_03_23_121410_create_servicos_table', 1),
('2015_03_23_121419_create_contato_table', 1),
('2015_03_23_121434_create_contatos_recebidos_table', 1),
('2015_03_23_151503_create_cabecalhos_table', 1),
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_23_121033_create_chamadas_table', 1),
('2015_03_23_121057_create_banners_table', 1),
('2015_03_23_121212_create_home_table', 1),
('2015_03_23_121325_create_institucional_table', 1),
('2015_03_23_121342_create_diferenciais_table', 1),
('2015_03_23_121410_create_servicos_table', 1),
('2015_03_23_121419_create_contato_table', 1),
('2015_03_23_121434_create_contatos_recebidos_table', 1),
('2015_03_23_151503_create_cabecalhos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `servicos_pagina_unique` (`pagina`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `pagina`, `titulo`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'gestao-estrategica', 'Gestão Estratégica', '<ul>\r\n	<li>A consultoria aplicada &agrave; camada estrat&eacute;gica identifica os fatores cr&iacute;ticos de sucesso do neg&oacute;cio e busca impulsionadores para a melhoria de seu desempenho, com isso gera benef&iacute;cios para a organiza&ccedil;&atilde;o, como melhorias nos resultados financeiros, operacionais e&nbsp;de mercado.</li>\r\n	<li>A utiliza&ccedil;&atilde;o do BSC (Balanced Scorecard) permite a conex&atilde;o entre o planejamento estrat&eacute;gico e a realidade cotidiana por meio dos indicadores (KPIs) que s&atilde;o identificados e monitorados.</li>\r\n	<li>Por fim visa materializar os objetivos estrat&eacute;gicos, conectando-os &agrave; arquitetura operacional da empresa e identificando os processos cr&iacute;ticos afetados pela estrat&eacute;gia.</li>\r\n</ul>\r\n\r\n<h3><strong>VIS&Atilde;O GERAL DOS INDICADORES</strong></h3>\r\n', '20150410180645_visao-geral.png', 0, '2015-04-10 21:06:45', '2015-04-17 16:54:56'),
(2, 'gestao-de-processos', 'Gestão de Processos', '<ul>\r\n	<li>A consultoria utiliza a forma combinada e integrada a otimiza&ccedil;&atilde;o operacional atrav&eacute;s do BPM &ndash; Multidimencional, a gest&atilde;o de performance e a automa&ccedil;&atilde;o operacional.</li>\r\n	<li>Identificado as oportunidades de melhoria e utilizando sua vasta biblioteca de modelos de arquitetura empresarial e processos de neg&oacute;cio com referenciais de melhores pr&aacute;ticas, a 5A Consultoria conduz seus clientes ao estado de alinhamento cont&iacute;nuo entre as gest&otilde;es estrat&eacute;gica, financeira, mercadol&oacute;gica e operacional.</li>\r\n	<li>Com Base na arquitetura da camada operacional, a 5A Consultoria ajusta a aplica&ccedil;&atilde;o de tecnologia da informa&ccedil;&atilde;o, garantindo a melhor forma de utiliza&ccedil;&atilde;o dos recursos de sistemas aplicativos, sem a custumeira pervers&atilde;o da opera&ccedil;&atilde;o do neg&oacute;cio.</li>\r\n	<li>Com a capacita&ccedil;&atilde;o para atuar em diversas plataformas, realizamos o desenvolvimento de novas aplica&ccedil;&otilde;es ou tuning, garantindo o atendimento dos objetivos de melhoria do neg&oacute;cio.</li>\r\n</ul>\r\n', '20150410181108_1.png', 1, '2015-04-10 21:11:08', '2015-04-17 16:52:34'),
(3, 'gestao-de-projetos', 'Gestão de Projetos', '<ul>\r\n	<li>O gerenciamento de projetos tem sido adotado, cada vez mais, por diversas empresas no Brasil e no mundo, quando precisam administrar suas atividades n&atilde;o rotineiras e de inova&ccedil;&atilde;o. N&atilde;o &eacute; incomum ver que a dissemina&ccedil;&atilde;o das pr&aacute;ticas de gerenciamento de projetos &eacute; feita de forma inadequada, gerando investimentos excessivos e, muitas vezes, fazendo-se com que os incentivos sejam transformados em perdas de oportunidades.</li>\r\n	<li>Para a 5A Consultoria um escrit&oacute;rio de projetos &eacute; uma organiza&ccedil;&atilde;o que incorpora o gerenciamento de projetos de uma empresa, podendo, esta incorpora&ccedil;&atilde;o ser parcial ou total, dependendo da op&ccedil;&atilde;o e necessidades dos clientes.</li>\r\n	<li>As car&ecirc;ncias em gerenciamento de projetos verificados nas empresas brasileiras s&atilde;o muitas e t&ecirc;m diversas origens, gerando assim, uma tipologia de escrit&oacute;rios de projetos, para atend&ecirc;-las.</li>\r\n	<li>Um escrit&oacute;rio de projetos &eacute; constitu&iacute;do de compet&ecirc;ncias diversas que, em linhas gerais, s&atilde;o exploradas em tr&ecirc;s dimens&otilde;es distintas e complementares &ndash; demanda, portf&oacute;lio e projetos.</li>\r\n	<li>As dimens&otilde;es s&atilde;o relacionadas e consecutivas e seguem a ordem apresentada na figura abaixo, sendo a sequ&ecirc;ncia gera decis&otilde;es e aprova&ccedil;&otilde;es de oportunidades e investimento alinhados com os objetivos da organiza&ccedil;&atilde;o.</li>\r\n</ul>\r\n', '20150410181307_2.png', 2, '2015-04-10 21:13:07', '2015-04-17 16:52:45'),
(4, 'treinamento', 'Treinamento', '<p>Realizamos treinamento em:</p>\r\n\r\n<ul>\r\n	<li>Construindo o Planejamento Estrat&eacute;gico</li>\r\n	<li>Fundamentos do BSC</li>\r\n	<li>Modelagem de Processos de Neg&oacute;cios &ndash; BPM-M</li>\r\n	<li>Como implantar um PMO</li>\r\n</ul>\r\n', '0', 3, '2015-04-10 21:13:26', '2015-04-10 21:13:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$Z8/OCpIWnBwJojhNmhdXAOE3nnTH9nh7C5eEp6Mbin1NWYIlRuVoG', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
