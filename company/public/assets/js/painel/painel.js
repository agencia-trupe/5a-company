$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	var result = confirm("Deseja Excluir o Registro?");
      	if(result) form.submit();
  	});

    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post(BASE + '/painel/ajax/order', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    var TEXTAREA_CONFIG = {
        endereco: {
            toolbar: []
        },
        texto: {
            toolbar: [['Bold', 'Italic'], ['BulletedList']]
        }
    };

    $('.ckeditor').each(function (i, obj) {
        console.log(obj.name);
        CKEDITOR.replace(obj.id, TEXTAREA_CONFIG[obj.name]);
    });

});
