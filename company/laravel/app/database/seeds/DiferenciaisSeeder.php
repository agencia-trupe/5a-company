<?php

class DiferenciaisSeeder extends Seeder {

    public function run()
    {
        DB::table('diferenciais')->delete();

        $data = array(
            array(
                'texto' => '...'
            )
        );

        DB::table('diferenciais')->insert($data);
    }

}
