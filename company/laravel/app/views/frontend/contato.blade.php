@section('content')

    <main id="contato" class="center">
        <div class="cabecalho">
            <h2>Contato</h2>
            <img src="{{ asset('../assets/img/cabecalhos/'.$cabecalho->contato) }}" alt="">
        </div>

        <div class="content">
            <div class="info">
                <p class="telefone">{{ $contato->telefone }}</p>
                <div class="endereco">{{ $contato->endereco }}</div>
                <div class="googlemaps">
                    {{ $contato->googlemaps }}
                </div>
            </div>

            <form action="" method="post" id="form-contato">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="Enviar">

                <div id="form-resposta"></div>
            </form>
        </div>
    </main>
@stop