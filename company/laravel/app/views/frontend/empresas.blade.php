@section('content')

    <main id="empresas" class="center">
        <div class="cabecalho">
            <h2>Empresas do Grupo</h2>
            <img src="{{ asset('../assets/img/cabecalhos/'.$cabecalho->empresas) }}" alt="">
        </div>

        <div class="content">
            @foreach($empresas as $empresa)
            <a href="{{ $empresa->link }}" target="_blank">
                <div class="imagem">
                    <img src="{{ asset('../assets/img/empresas/'.$empresa->imagem) }}" alt="{{ $empresa->nome }}">
                </div>
                <div class="descricao">
                    <p>{{ $empresa->descricao }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </main>
@stop
