@section('content')

    <legend>
        <h2><small>Home /</small> Adicionar Banner</h2>
    </legend>

    {{ Form::open(['route' => 'painel.banners.store', 'files' => true]) }}

        @include('painel.banners._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
