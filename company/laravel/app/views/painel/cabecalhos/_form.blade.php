@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="well form-group">
    {{ Form::label('institucional', 'Institucional') }}
@if($submitText == 'Alterar')
    <img src="{{ url('../assets/img/cabecalhos/'.$cabecalhos->institucional) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('institucional', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('diferenciais', 'Diferenciais') }}
@if($submitText == 'Alterar')
    <img src="{{ url('../assets/img/cabecalhos/'.$cabecalhos->diferenciais) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('diferenciais', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('empresas', 'Empresas do Grupo') }}
@if($submitText == 'Alterar')
    <img src="{{ url('../assets/img/cabecalhos/'.$cabecalhos->empresas) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('empresas', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('contato', 'Contato') }}
@if($submitText == 'Alterar')
    <img src="{{ url('../assets/img/cabecalhos/'.$cabecalhos->contato) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('contato', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}
