@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Empresas do Grupo
            <a href="{{ route('painel.empresas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Empresa</a>
        </h2>
    </legend>

    @if(count($empresas))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="empresas">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th>Nome</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($empresas as $empresa)

            <tr class="tr-row" id="id_{{ $empresa->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ url('../assets/img/empresas/'.$empresa->imagem) }}" alt="" style="width:100%;max-width:180px;height:auto;"></td>
                <td>{{ $empresa->nome }}</td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.empresas.destroy', $empresa->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.empresas.edit', $empresa->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma empresa cadastrada.</div>
    @endif

@stop
