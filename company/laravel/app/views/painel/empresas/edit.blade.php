@section('content')

    <legend>
        <h2><small>Empresas do Grupo /</small> Editar Empresa</h2>
    </legend>

    {{ Form::model($empresa, [
        'route' => ['painel.empresas.update', $empresa->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.empresas._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
