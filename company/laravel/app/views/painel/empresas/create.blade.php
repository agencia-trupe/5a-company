@section('content')

    <legend>
        <h2><small>Empresas do Grupo /</small> Adicionar Empresa</h2>
    </legend>

    {{ Form::open(['route' => 'painel.empresas.store', 'files' => true]) }}

        @include('painel.empresas._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
