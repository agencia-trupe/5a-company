<?php

use \Empresa;

class EmpresasController extends BaseController {

	public function index()
	{
        $empresas = Empresa::ordenados()->get();

        return $this->view('frontend.empresas', compact('empresas'));
	}

}
