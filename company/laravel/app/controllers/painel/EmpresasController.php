<?php

namespace Painel;

use \Empresa, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class EmpresasController extends BasePainelController {

    public function index()
    {
        $empresas = Empresa::ordenados()->get();

        return $this->view('painel.empresas.index', compact('empresas'));
    }

    public function create()
    {
        return $this->view('painel.empresas.create');
    }

    public function store()
    {
        $input = Input::all();
        $rules = Empresa::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', Empresa::$imagem_config);
            Empresa::create($input);
            Session::flash('sucesso', 'Empresa criada com sucesso.');

            return Redirect::route('painel.empresas.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar empresa.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $empresa = Empresa::findOrFail($id);

        return $this->view('painel.empresas.edit', compact('empresa'));
    }

    public function update($id)
    {
        $empresa = Empresa::findOrFail($id);
        $input   = Input::all();
        $rules   = Empresa::$rules;

        $rules['imagem'] = 'image';

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', Empresa::$imagem_config);
            } else {
                unset($input['imagem']);
            }

            $empresa->update($input);
            Session::flash('sucesso', 'Empresa alterada com sucesso.');

            return Redirect::route('painel.empresas.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar empresa.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Empresa::destroy($id);
            Session::flash('sucesso', 'Empresa removida com sucesso.');

            return Redirect::route('painel.empresas.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover empresa.']);

        }
    }

}
