<?php

namespace Painel;

use \Chamada, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class ChamadasController extends BasePainelController {

    public function index()
    {
        $chamadas = Chamada::ordenados()->get();

        return $this->view('painel.chamadas.index', compact('chamadas'));
    }

    public function edit($id)
    {
        $chamada = Chamada::findOrFail($id);

        return $this->view('painel.chamadas.edit', compact('chamada'));
    }

    public function update($id)
    {
        $chamada = Chamada::findOrFail($id);
        $input   = Input::all();
        $rules   = Chamada::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', Chamada::$imagem_config);
            } else {
                unset($input['imagem']);
            }

            $chamada->update($input);
            Session::flash('sucesso', 'Chamada alterada com sucesso.');

            return Redirect::route('painel.chamadas.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar chamada.'])
                ->withInput();

        }
    }

}
