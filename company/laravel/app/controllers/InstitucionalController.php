<?php

use \Institucional;

class InstitucionalController extends BaseController {

    public function index($pagina = 'descritivo')
    {
        $institucional = Institucional::select(['pagina', 'titulo'])->get();

        $pagina = Institucional::pagina($pagina)->first();
        if (!$pagina) App::abort('404');

        return $this->view('frontend.institucional', compact('institucional', 'pagina'));
    }

}
