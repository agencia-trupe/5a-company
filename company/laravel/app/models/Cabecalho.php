<?php

class Cabecalho extends Eloquent
{

    protected $table = 'cabecalhos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'institucional' => 'image',
        'diferenciais'  => 'image',
        'empresas'      => 'image',
        'contato'       => 'image'
    ];

    public static $imagem_config = [
        'width'  => 414,
        'height' => 168,
        'path'   => 'assets/img/cabecalhos/'
    ];

}
