<?php

class Empresa extends Eloquent
{

    protected $table = 'empresas';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'nome'       => 'required',
        'descricao'  => 'required',
        'link'       => 'required',
        'imagem'     => 'required|image'
    ];

    public static $imagem_config = [
        'width'  => 250,
        'height' => null,
        'path'   => 'assets/img/empresas/',
        'upsize' => true
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
