<?php

class Diferenciais extends Eloquent
{

    protected $table = 'diferenciais';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'texto' => 'required',
    ];

}
