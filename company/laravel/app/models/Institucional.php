<?php

class Institucional extends Eloquent
{

    protected $table = 'institucional';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'texto' => 'required',
        'imagem' => 'image'
    ];

    public static $imagem_config = [
        'width'  => 450,
        'height' => null,
        'path'   => 'assets/img/institucional/',
        'upsize' => true
    ];

    public function scopePagina($query, $pagina)
    {
        return $query->where('pagina', '=', $pagina);
    }

}
