-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17-Abr-2015 às 15:46
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `5a-company`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '20150417132852_shutterstock_53472508.jpg', -1, '2015-04-15 15:20:06', '2015-04-17 16:28:55'),
(2, '20150417132909_shutterstock_87318925.jpg', -1, '2015-04-15 15:20:12', '2015-04-17 16:29:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cabecalhos`
--

CREATE TABLE IF NOT EXISTS `cabecalhos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institucional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cabecalhos`
--

INSERT INTO `cabecalhos` (`id`, `institucional`, `diferenciais`, `empresas`, `contato`, `created_at`, `updated_at`) VALUES
(1, '20150417134415_SalesHandShake-2.jpg', '20150417133310_shutterstock_1457803 (2).jpg', '20150417133431_shutterstock_53472508.jpg', '20150417133433_shutterstock_3389291.jpg', '0000-00-00 00:00:00', '2015-04-17 16:44:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE IF NOT EXISTS `chamadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `link`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'http://www.5aconsultorianet.com.br', '5A Consultoria', '20150409192134_consultoria.png', 0, '0000-00-00 00:00:00', '2015-04-09 22:38:19'),
(2, 'http://www.globalcsc.com.br', 'GlobalCSC', '20150409192307_globalcsc.png', 2, '0000-00-00 00:00:00', '2015-04-09 22:39:00'),
(3, 'http://www.essencenet.com.br', 'Essence', '20150409192229_essence.png', 3, '0000-00-00 00:00:00', '2015-04-09 22:39:19'),
(4, 'http://www.5a.com.br', '5A Gestão de Talentos', '20150409192233_gestao.png', 1, '0000-00-00 00:00:00', '2015-04-09 22:39:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `googlemaps`, `facebook`, `twitter`, `created_at`, `updated_at`) VALUES
(1, 'contato@5a.com.br', '11 2344-4596', '<p>Alphaville</p>\r\n\r\n<p>Alameda Araguaia n&ordm; 2.190 -&nbsp;Torre I -&nbsp;3&ordm; Andar</p>\r\n\r\n<p>Barueri -&nbsp;06455-000</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.877678294674!2d-46.84094360000001!3d-23.500914999999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf021eb8ca5113%3A0x951f830123ad2e0f!2sAlameda+Araguaia%2C+2190+-+Alphaville+Industrial%2C+Barueri+-+SP!5e0!3m2!1spt-BR!2sbr!4v1428608132921" width="100%" height="100%" frameborder="0" style="border:0"></iframe>', 'http://www.facebook.com', 'http://www.twitter.com', '0000-00-00 00:00:00', '2015-04-09 22:35:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `diferenciais`
--

CREATE TABLE IF NOT EXISTS `diferenciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `diferenciais`
--

INSERT INTO `diferenciais` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Melhor organiza&ccedil;&atilde;o administrativa dos processos e neg&oacute;cios.</p>\r\n\r\n<p>Fortalecimento da implanta&ccedil;&atilde;o da governan&ccedil;a corporativa.</p>\r\n\r\n<p>Redu&ccedil;&atilde;o de custos para as empresas do grupo.</p>\r\n', '0000-00-00 00:00:00', '2015-04-09 22:33:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id`, `link`, `nome`, `descricao`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'http://www.5aconsultorianet.com.br', '5A Consultoria', 'Atua na implantação do modelo sírius que propicia o alinhamento entre a estratégia e a operação.', '20150410124400_consultoria_g.png', 0, '2015-04-10 15:35:45', '2015-04-10 15:44:00'),
(2, 'http://www.globalcsc.com.br', 'GlobalCSC', 'Atua na terceirização dos processos de BackOffice como Administrativo, Financeiro e TI.', '20150410124413_globalcsc_g.png', 2, '2015-04-10 15:40:03', '2015-04-10 15:44:13'),
(3, 'http://www.essencenet.com.br', 'Essence', 'Atua na terceirização dos processos de gestão ocupacional, segurança do trabalho e qualidade de vida.', '20150410124421_essence_g.png', 3, '2015-04-10 15:40:41', '2015-04-10 15:44:21'),
(4, 'http://www.5a.com.br', '5A Gestão de Talentos', 'Atua na consultoria e terceirização dos processos de contratação, gestão e retenção de talentos.', '20150410124408_gestao_g.png', 1, '2015-04-10 15:41:09', '2015-04-10 15:44:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `frase`, `created_at`, `updated_at`) VALUES
(1, 'A 5A Company é uma sociedade gestora de participações sociais que administra atualmente 4 empresas.', '0000-00-00 00:00:00', '2015-04-15 15:18:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `institucional`
--

CREATE TABLE IF NOT EXISTS `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `institucional`
--

INSERT INTO `institucional` (`id`, `pagina`, `titulo`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'descritivo', 'Descritivo', '<p>A 5A Company &eacute; uma sociedade gestora de participa&ccedil;&otilde;es sociais que administra atualmente 4 empresas.</p>\r\n\r\n<p>De origem brasileira, fundada em 2010, &eacute; reconhecida pelos resultados alcan&ccedil;ados com a implementa&ccedil;&atilde;o do Modelo Sirius em suas empresas.</p>\r\n\r\n<p>Atua&nbsp;com foco em diversos segmentos de mercado, propiciando as empresas do grupo uma adequa&ccedil;&atilde;o perfeita do seu modelo de neg&oacute;cio.</p>\r\n', '', '0000-00-00 00:00:00', '2015-04-17 16:27:55'),
(2, 'missao', 'Missão', '<p>A 5A Company tem como miss&atilde;o melhorar a estrutura de capital de suas empresas mantendo e criando mecanismos que evoluam as parceria e a aquisi&ccedil;&atilde;o de novas empresas.</p>\r\n', '20150417134233_shutterstock_51955312.jpg', '0000-00-00 00:00:00', '2015-04-17 16:42:35'),
(3, 'valores', 'Valores', '<ul>\r\n	<li>Verdade</li>\r\n	<li>Transpar&ecirc;ncia</li>\r\n	<li>Responsabilidade</li>\r\n	<li>Equil&iacute;brio</li>\r\n	<li>Inova&ccedil;&atilde;o</li>\r\n</ul>\r\n', '20150417133740_shutterstock_62797204.jpg', '0000-00-00 00:00:00', '2015-04-17 16:37:44'),
(4, 'origem-do-nome', 'Origem do nome', '<p>Para que uma empresa suportada por equipes alcance os resultados esperados, &eacute; necess&aacute;rio que o Talento Humano (ser humano) esteja orientado a manter a <strong>Aten&ccedil;&atilde;o </strong>no que faz e no que acontece ao seu redor, identificando as oportunidades e os obst&aacute;culos, dessa forma ter&aacute; possibilidade de criar <strong>Alternativas </strong>que aproveitem as oportunidades e superem os obst&aacute;culos. Uma Alternativa deve sempre ser acompanhada de uma boa dose de <strong>Atitude</strong>, permitindo assim a sua realiza&ccedil;&atilde;o, por sua vez, as Alternativas devem buscar um n&iacute;vel de <strong>Assertividade</strong>. Por&eacute;m de nada adiantar&aacute; a Aten&ccedil;&atilde;o, a gera&ccedil;&atilde;o de Alternativas, a Atitude e a Assertividade se o talento humano n&atilde;o <strong>Acreditar </strong>no que se prop&otilde;e a fazer na vida profissional e pessoal.</p>\r\n\r\n<p>Desta forma, estes 5 elementos que diferenciam um profissional de outro, s&atilde;o os elementos fundamentais que fazem parte do pr&oacute;prio nome da empresa.</p>\r\n', '20150409193027_origem.jpg', '0000-00-00 00:00:00', '2015-04-09 22:30:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_23_121033_create_chamadas_table', 1),
('2015_03_23_121057_create_banners_table', 1),
('2015_03_23_121212_create_home_table', 1),
('2015_03_23_121325_create_institucional_table', 1),
('2015_03_23_121342_create_diferenciais_table', 1),
('2015_03_23_121419_create_contato_table', 1),
('2015_03_23_121434_create_contatos_recebidos_table', 1),
('2015_03_23_151503_create_cabecalhos_table', 1),
('2015_04_10_121638_create_empresas_table', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$ENeeAXT5WksJjDe30w4tn.wzj7O4hB5KKfGYbGWNlN872MXsP64NO', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
