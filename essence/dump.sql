-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17-Abr-2015 às 19:09
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `5a-essence`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '20150415130511_shutterstock_86793187.jpg', -1, '2015-04-13 17:47:24', '2015-04-15 16:05:13'),
(2, '20150415130518_shutterstock_8509111.jpg', -1, '2015-04-15 16:05:22', '2015-04-15 16:05:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cabecalhos`
--

CREATE TABLE IF NOT EXISTS `cabecalhos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institucional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servicos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cabecalhos`
--

INSERT INTO `cabecalhos` (`id`, `institucional`, `diferenciais`, `servicos`, `contato`, `created_at`, `updated_at`) VALUES
(1, '20150417170730_etC67wiTpoA.jpg', '20150417170558_MTI3NTgyNTI5OTI5ODE2NzA3.jpg', '20150417170454_20150415130834_shutterstock_17944492.jpg', '20150415130808_shutterstock_74305498.jpg', '0000-00-00 00:00:00', '2015-04-17 20:07:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE IF NOT EXISTS `chamadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `link`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'servicos/seguranca-do-trabalho', 'Segurança do Trabalho', '20150415131350_shutterstock_2030425 (2).jpg', -1, '0000-00-00 00:00:00', '2015-04-17 20:09:06'),
(2, 'servicos/medicina-do-trabalho', 'Medicina do Trabalho', '20150415131400_shutterstock_23907682.jpg', -1, '0000-00-00 00:00:00', '2015-04-17 20:09:12'),
(3, 'servicos/qualidade-de-vida', 'Qualidade de Vida', '20150415131406_shutterstock_53410885.jpg', -1, '0000-00-00 00:00:00', '2015-04-17 20:09:19'),
(4, 'servicos/gestao-ambiental', 'Gestão Ambiental', '20150415131412_shutterstock_62797204.jpg', -1, '0000-00-00 00:00:00', '2015-04-17 20:09:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `googlemaps`, `facebook`, `twitter`, `created_at`, `updated_at`) VALUES
(1, 'contato@5a.com.br', '11 4508-2332', '<p>S&atilde;o Paulo</p>\r\n\r\n<p>Rua Bar&atilde;o de Itapetininga, 246 - 9&ordm; Andar&nbsp;</p>\r\n\r\n<p>Rep&uacute;blica - 01042-901</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3657.676491925239!2d-46.6412121!3d-23.5441353!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5851d82fc3cd%3A0x7a2aaebcb62d24b8!2sR.+Bar%C3%A3o+de+Itapetininga%2C+246+-+Rep%C3%BAblica%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1428936791454" width="100%" height="100%" frameborder="0" style="border:0"></iframe>', 'http://www.facebook.com', 'http://www.twitter.com', '0000-00-00 00:00:00', '2015-04-13 17:53:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `diferenciais`
--

CREATE TABLE IF NOT EXISTS `diferenciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `diferenciais`
--

INSERT INTO `diferenciais` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<h3>ACRE</h3>\r\n\r\n<ul>\r\n	<li>Rio Branco</li>\r\n</ul>\r\n\r\n<h3>ALAGOAS</h3>\r\n\r\n<ul>\r\n	<li>Macei&oacute;</li>\r\n</ul>\r\n\r\n<h3>AMAP&Aacute;</h3>\r\n\r\n<ul>\r\n	<li>Macap&aacute;</li>\r\n</ul>\r\n\r\n<h3>AMAZONAS</h3>\r\n\r\n<ul>\r\n	<li>Manaus</li>\r\n</ul>\r\n\r\n<h3>BAHIA</h3>\r\n\r\n<ul>\r\n	<li>Cama&ccedil;ari</li>\r\n	<li>Candeias</li>\r\n	<li>Feira de Santana</li>\r\n	<li>Itabuna</li>\r\n	<li>Lauro de Freitas</li>\r\n	<li>Lu&iacute;s Eduardo Magalh&atilde;es</li>\r\n	<li>Salvador</li>\r\n	<li>Seara</li>\r\n	<li>Vit&oacute;ria da Conquista</li>\r\n</ul>\r\n\r\n<h3>CEAR&Aacute;</h3>\r\n\r\n<ul>\r\n	<li>Fortaleza</li>\r\n	<li>Juazeiro do Norte</li>\r\n	<li>Mossor&oacute;</li>\r\n</ul>\r\n\r\n<h3>DISTRITO FEDERAL</h3>\r\n\r\n<ul>\r\n	<li>Bras&iacute;lia</li>\r\n</ul>\r\n\r\n<h3>ESPIRITO SANTO</h3>\r\n\r\n<ul>\r\n	<li>Colatina</li>\r\n	<li>Santo Agostinho</li>\r\n	<li>Serra</li>\r\n	<li>Vila Velha</li>\r\n	<li>Vit&oacute;ria</li>\r\n</ul>\r\n\r\n<h3>GOI&Aacute;S</h3>\r\n\r\n<ul>\r\n	<li>Catal&atilde;o</li>\r\n	<li>Goiania</li>\r\n	<li>Itumbiara</li>\r\n	<li>Rio Verde</li>\r\n</ul>\r\n\r\n<h3>MARANH&Atilde;O</h3>\r\n\r\n<ul>\r\n	<li>A&ccedil;ail&acirc;ndia</li>\r\n	<li>Imperatriz</li>\r\n	<li>S&atilde;o Lu&iacute;s</li>\r\n</ul>\r\n\r\n<h3>MATO GROSSO</h3>\r\n\r\n<ul>\r\n	<li>Cuiab&aacute;</li>\r\n	<li>Diamantino</li>\r\n	<li>Rondon&oacute;polis</li>\r\n	<li>Sorriso</li>\r\n</ul>\r\n\r\n<h3>MATO GROSSO DO SUL</h3>\r\n\r\n<ul>\r\n	<li>Campo Grande</li>\r\n	<li>Dourados</li>\r\n	<li>Tr&ecirc;s Lagoas</li>\r\n</ul>\r\n\r\n<h3>MINAS GERAIS</h3>\r\n\r\n<ul>\r\n	<li>Arax&aacute;</li>\r\n	<li>Barbacena</li>\r\n	<li>Belo Horizonte</li>\r\n	<li>Cataguases</li>\r\n	<li>Contagem</li>\r\n	<li>Divin&oacute;polis</li>\r\n	<li>Extrema</li>\r\n	<li>Governador Valadares</li>\r\n	<li>Hortol&acirc;ndia</li>\r\n	<li>Ipatinga</li>\r\n	<li>Itauna</li>\r\n	<li>Jo&atilde;o Monlevade</li>\r\n	<li>Juiz de Fora&nbsp;</li>\r\n	<li>Leopoldina&nbsp;</li>\r\n	<li>Montes Claros</li>\r\n	<li>Paracatu</li>\r\n	<li>Passos&nbsp;</li>\r\n	<li>Patos de Minas</li>\r\n	<li>Po&ccedil;os de Caldas</li>\r\n	<li>Pouso Alegre&nbsp;</li>\r\n	<li>Tim&oacute;teo</li>\r\n	<li>Uberaba</li>\r\n	<li>Varginha</li>\r\n	<li>Vespasiano</li>\r\n</ul>\r\n\r\n<h3>PAR&Aacute;</h3>\r\n\r\n<ul>\r\n	<li>Bel&eacute;m</li>\r\n</ul>\r\n\r\n<h3>PARA&Iacute;BA</h3>\r\n\r\n<ul>\r\n	<li>Campina Grande</li>\r\n	<li>Jo&atilde;o Pessoa</li>\r\n</ul>\r\n\r\n<h3>PARAN&Aacute;</h3>\r\n\r\n<ul>\r\n	<li>Arauc&aacute;ria</li>\r\n	<li>Cascavel</li>\r\n	<li>Contenda</li>\r\n	<li>Curitiba</li>\r\n	<li>Foz do Igua&ccedil;u</li>\r\n	<li>Francisco Beltr&atilde;o</li>\r\n	<li>Guarapuava</li>\r\n	<li>Londrina&nbsp;</li>\r\n	<li>Maring&aacute;&nbsp;</li>\r\n	<li>Paranagu&aacute;&nbsp;</li>\r\n	<li>Ponta Grossa</li>\r\n	<li>S&atilde;o Jos&eacute; dos Pinhais</li>\r\n	<li>Umuarama</li>\r\n</ul>\r\n\r\n<h3>PERNAMBUCO</h3>\r\n\r\n<ul>\r\n	<li>Cabo de Santo</li>\r\n	<li>Cachoeirinha</li>\r\n	<li>Cachoeiro de Itapemirim</li>\r\n	<li>Caruaru</li>\r\n	<li>Petrolina</li>\r\n	<li>Recife</li>\r\n</ul>\r\n\r\n<h3>PIAU&Iacute;</h3>\r\n\r\n<ul>\r\n	<li>Teresina</li>\r\n</ul>\r\n\r\n<h3>RIO DE JANEIRO</h3>\r\n\r\n<ul>\r\n	<li>Campos dos goytacazes</li>\r\n	<li>Duque de Caxias</li>\r\n	<li>Itagua&iacute;</li>\r\n	<li>Maca&eacute;</li>\r\n	<li>Niter&oacute;i</li>\r\n	<li>Nova Friburgo</li>\r\n	<li>Nova Igua&ccedil;u</li>\r\n	<li>Petr&oacute;polis</li>\r\n	<li>Resende</li>\r\n	<li>Rio de Janeiro</li>\r\n</ul>\r\n\r\n<h3>RIO GRANDE DO NORTE</h3>\r\n\r\n<ul>\r\n	<li>Natal</li>\r\n	<li>Parnamirim</li>\r\n</ul>\r\n\r\n<h3>RIO GRANDE DO SUL</h3>\r\n\r\n<ul>\r\n	<li>Bag&eacute;</li>\r\n	<li>Ca&ccedil;apava do Sul</li>\r\n	<li>Canoas</li>\r\n	<li>Carlos Barbosa</li>\r\n	<li>Caxias do Sul</li>\r\n	<li>Frederico Westphalen</li>\r\n	<li>Gramado</li>\r\n	<li>Lajeado</li>\r\n	<li>Montenegro</li>\r\n	<li>Novo Hamburgo</li>\r\n	<li>Passo Fundo</li>\r\n	<li>Pelotas</li>\r\n	<li>Porto Alegre</li>\r\n	<li>Rio Grande</li>\r\n	<li>Santa Cruz do Sul</li>\r\n	<li>Santa Maria</li>\r\n	<li>Santo &Acirc;ngelo</li>\r\n	<li>Tr&ecirc;s Passos</li>\r\n	<li>Uruguaiana</li>\r\n	<li>Porto Velho</li>\r\n</ul>\r\n\r\n<h3>ROND&Ocirc;NIA</h3>\r\n\r\n<ul>\r\n	<li>Porto Velho</li>\r\n</ul>\r\n\r\n<h3>RORAIMA</h3>\r\n\r\n<ul>\r\n	<li>Boa Vista</li>\r\n</ul>\r\n\r\n<h3>SANTA CATARINA</h3>\r\n\r\n<ul>\r\n	<li>Balne&aacute;rio Cambori&uacute;</li>\r\n	<li>Blumenau</li>\r\n	<li>Chapec&oacute;</li>\r\n	<li>Crici&uacute;ma&nbsp;</li>\r\n	<li>Florian&oacute;polis&nbsp;</li>\r\n	<li>Itaja&iacute;</li>\r\n	<li>Itapiranga</li>\r\n	<li>Jaragu&aacute; do Sul</li>\r\n	<li>Joinville</li>\r\n	<li>Navegantes</li>\r\n	<li>Rio do Sul</li>\r\n	<li>S&atilde;o Francisco do Sul</li>\r\n</ul>\r\n\r\n<h3>S&Atilde;O PAULO</h3>\r\n\r\n<ul>\r\n	<li>Americana</li>\r\n	<li>Amparo</li>\r\n	<li>Ara&ccedil;atuba</li>\r\n	<li>Araraquara</li>\r\n	<li>Atibaia</li>\r\n	<li>Avar&eacute;</li>\r\n	<li>Barretos</li>\r\n	<li>Barueri</li>\r\n	<li>Bauru</li>\r\n	<li>Boituva</li>\r\n	<li>Botucatu</li>\r\n	<li>Bragan&ccedil;a Paulista</li>\r\n	<li>Campinas</li>\r\n	<li>Cotia</li>\r\n	<li>Cubat&atilde;o</li>\r\n	<li>Diadema</li>\r\n	<li>Dracena</li>\r\n	<li>Embu</li>\r\n	<li>Fernand&oacute;polis</li>\r\n	<li>Franca</li>\r\n	<li>Gar&ccedil;a</li>\r\n	<li>Guaratinguet&aacute;</li>\r\n	<li>Guarulhos</li>\r\n	<li>Indaiatuba</li>\r\n	<li>Iracem&aacute;polis</li>\r\n	<li>Itanha&eacute;m</li>\r\n	<li>Itapevi</li>\r\n	<li>Jacare&iacute;</li>\r\n	<li>Jaguari&uacute;na</li>\r\n	<li>Ja&uacute;</li>\r\n	<li>Jundia&iacute;</li>\r\n	<li>Len&ccedil;&oacute;is Paulista</li>\r\n	<li>Limeira</li>\r\n	<li>Mar&iacute;lia</li>\r\n	<li>Mogi das Cruzes</li>\r\n	<li>Mogi Gua&ccedil;u</li>\r\n	<li>Orl&acirc;ndia</li>\r\n	<li>Osasco</li>\r\n	<li>Ourinhos</li>\r\n	<li>Paragua&ccedil;u Paulista</li>\r\n	<li>Paul&iacute;nia</li>\r\n	<li>Piracicaba</li>\r\n	<li>Praia Grande</li>\r\n	<li>Presidente Prudente</li>\r\n	<li>Ribeir&atilde;o Preto</li>\r\n	<li>Santo Andr&eacute;</li>\r\n	<li>Santos</li>\r\n	<li>S&atilde;o Bernardos do Campo</li>\r\n	<li>S&atilde;o Caetano do Sul</li>\r\n	<li>S&atilde;o Jos&eacute; do Rio Preto&nbsp;</li>\r\n	<li>S&atilde;o Jos&eacute; dos Campos</li>\r\n	<li>S&atilde;o Paulo</li>\r\n	<li>S&atilde;o Sebasti&atilde;o</li>\r\n	<li>Sorocaba</li>\r\n	<li>Suzano&nbsp;</li>\r\n	<li>Taubat&eacute;&nbsp;</li>\r\n	<li>Valinhos</li>\r\n</ul>\r\n\r\n<h3>SERGIPE</h3>\r\n\r\n<ul>\r\n	<li>Aracaju</li>\r\n</ul>\r\n\r\n<h3>TOCANTINS</h3>\r\n\r\n<ul>\r\n	<li>Palmas</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-04-13 18:08:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `frase`, `created_at`, `updated_at`) VALUES
(1, 'A ESSENCE é uma empresa do Grupo 5A Company que atua na área de medicina e segurança do trabalho e gestão ambiental.', '0000-00-00 00:00:00', '2015-04-16 15:48:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `institucional`
--

CREATE TABLE IF NOT EXISTS `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `institucional`
--

INSERT INTO `institucional` (`id`, `pagina`, `titulo`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'descritivo', 'Descritivo', '<p>A <strong>ESSENCE </strong>&eacute; uma empresa do Grupo 5A Company &nbsp;que atua na &aacute;rea de medicina e seguran&ccedil;a do trabalho e gest&atilde;o ambiental.</p>\r\n\r\n<p>A <strong>ESSENCE </strong>nasce da significativa evolu&ccedil;&atilde;o das Pessoas, Processos e Sistemas, bem como, dos investimentos e desenvolvimento de novos produtos, servi&ccedil;os e m&eacute;todos de trabalho visando se diferenciar no mercado de presta&ccedil;&atilde;o de servi&ccedil;os de medicina e seguran&ccedil;a do trabalho fortalecendo a forma de atendimento, transpar&ecirc;ncia e atitude nas rela&ccedil;&otilde;es,&nbsp; clareza nas negocia&ccedil;&otilde;es e excel&ecirc;ncia nos servi&ccedil;os prestados.</p>\r\n\r\n<p>A <strong>ESSENCE </strong>leva ao mercado uma forma clara para organizar, otimizar e automatizar a estrutura de medicina e seguran&ccedil;a do trabalho de nossos clientes,&nbsp; gerando e gerenciando os planos de a&ccedil;&otilde;es que evitam diverg&ecirc;ncias junto aos &oacute;rg&atilde;os reguladores com o objetivo de redu&ccedil;&atilde;o de custos.&nbsp;</p>\r\n', '', '0000-00-00 00:00:00', '2015-04-17 18:21:45'),
(2, 'missao', 'Missão', '<p>Prestar servi&ccedil;o de excel&ecirc;ncia no setor de medicina e seguran&ccedil;a do trabalho monitorando a sa&uacute;de dos funcion&aacute;rios e o meio ambiente de nossos clientes, dentro de um local saud&aacute;vel e prop&iacute;cio para o aprendizado e crescimento de nossos colaboradores e&nbsp; garantindo um neg&oacute;cio rent&aacute;vel para os nossos investidores.</p>\r\n', '20150417170234_missao.jpg', '0000-00-00 00:00:00', '2015-04-17 20:02:34'),
(3, 'valores', 'Valores', '<ul>\r\n	<li>Entusiasmo</li>\r\n	<li>Supera&ccedil;&atilde;o</li>\r\n	<li>Simplicidade</li>\r\n	<li>Encantamento</li>\r\n	<li>Negocia&ccedil;&atilde;o</li>\r\n	<li>Coer&ecirc;ncia</li>\r\n	<li>Empreendedorismo</li>\r\n</ul>\r\n', '20150417170010_shutterstock_71826121.jpg', '0000-00-00 00:00:00', '2015-04-17 20:00:13'),
(4, 'origem-do-nome', 'Origem do nome', '<p>Acreditamos na evolu&ccedil;&atilde;o cont&iacute;nua das pessoas (ess&ecirc;ncia do ser) e processos&nbsp; (ess&ecirc;ncias das coisas);</p>\r\n\r\n<p>Entendemos que nossa atitude deve estar pautada na nossa palavra (n&atilde;o fazer com o pr&oacute;ximo o que n&atilde;o gostar&iacute;amos que fizessem conosco);</p>\r\n\r\n<p>Investimos no conhecimento com o objetivo de sermos distinguidos (sabedoria);</p>\r\n\r\n<p>Fortalecemos o relacionamento entre pessoas&nbsp; para alcan&ccedil;armos a excel&ecirc;ncia dos nossos servi&ccedil;os (integra&ccedil;&atilde;o);</p>\r\n', '', '0000-00-00 00:00:00', '2015-04-13 17:49:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_23_121033_create_chamadas_table', 1),
('2015_03_23_121057_create_banners_table', 1),
('2015_03_23_121212_create_home_table', 1),
('2015_03_23_121325_create_institucional_table', 1),
('2015_03_23_121342_create_diferenciais_table', 1),
('2015_03_23_121410_create_servicos_table', 1),
('2015_03_23_121419_create_contato_table', 1),
('2015_03_23_121434_create_contatos_recebidos_table', 1),
('2015_03_23_151503_create_cabecalhos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `servicos_pagina_unique` (`pagina`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `pagina`, `titulo`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'seguranca-do-trabalho', 'Segurança do Trabalho', '<h3>Introdu&ccedil;&atilde;o</h3>\r\n\r\n<p>As altera&ccedil;&otilde;es de legisla&ccedil;&atilde;o, das Normas Regulamentadoras e nas diretrizes estabelicidas pelo INSS t&ecirc;m levado as empresas a uma profunda reflex&atilde;o quanto aos consider&aacute;veis acr&eacute;scimos em suas despesas com pessoal. Nossos conceitos de disponibilizar solu&ccedil;&otilde;es personalizadas n&atilde;o somente atendem &agrave; legisla&ccedil;&atilde;o como permitem o gerenciamento das informa&ccedil;&otilde;es e a implementa&ccedil;&atilde;o das melhores solu&ccedil;&otilde;es, valorizando principalmente a sa&uacute;de, a seguran&ccedil;a, o conforto, a qualidade de vida e a produtividade. Todos esses investimentos s&atilde;o de alta rentabilidade e concorrem, de forma categ&oacute;rica, para a valoriza&ccedil;&atilde;o da imagem da empresa.</p>\r\n\r\n<h3>Programa de Preven&ccedil;&atilde;o de Riscos Ambientais</h3>\r\n\r\n<p>O Programa de Preven&ccedil;&atilde;o de Riscos Ambientais - PPRA, com obrigatoriedade de elabora&ccedil;&atilde;o, implementa&ccedil;&atilde;o e renova&ccedil;&atilde;o estabelecidas na NR-9, visa a preserva&ccedil;&atilde;o de sa&uacute;de e de integridade f&iacute;sica dos trabalhadores, atrav&eacute;s da antecipa&ccedil;&atilde;o, reconhecimento, avalia&ccedil;&atilde;o e conseq&uuml;ente controle da ocorr&ecirc;ncia de riscos ambientais existentes ou que venham a existir no ambiente de trabalho, tendo em considera&ccedil;&atilde;o a prote&ccedil;&atilde;o do meio ambiente e dos recursos naturais.</p>\r\n\r\n<p>A Essence com seu corpo de profissionais devidamente equipados, est&aacute; preparada para avaliar e inspecionar as condi&ccedil;&otilde;es ambientais da sua empresa, realizando levantamento dos riscos f&iacute;sicos, qu&iacute;micos e biol&oacute;gicos e seus reflexos no &acirc;mbito da integridade f&iacute;sica&nbsp; e das poss&iacute;veis doen&ccedil;as decorrentes. De posse de tais informa&ccedil;&otilde;es podem ser previstas a ado&ccedil;&atilde;o de medidas preventivas para atenuar ou neutralizar os riscos ambientais. Desta maneira a sua empresa estar&aacute; integrada no conjunto mais amplo das iniciativas no campo da preserva&ccedil;&atilde;o da sa&uacute;de e da integridade f&iacute;sica dos trabalhadores.</p>\r\n\r\n<p>Prev&ecirc; as seguintes etapas:</p>\r\n\r\n<ul>\r\n	<li>Antecipa&ccedil;&atilde;o e reconhecimento dos riscos;</li>\r\n	<li>Estabelecimento de prioridades e metas de avalia&ccedil;&atilde;o e controle;</li>\r\n	<li>Implanta&ccedil;&atilde;o de medidas de controle e avalia&ccedil;&atilde;o da sua efic&aacute;cia;</li>\r\n	<li>Monitoramento da exposi&ccedil;&atilde;o aos riscos;</li>\r\n	<li>Registro e divulga&ccedil;&atilde;o dos dados.</li>\r\n</ul>\r\n\r\n<h3>LTCAT-NR 15</h3>\r\n\r\n<p>A necessidade de se elaborar os Laudos T&eacute;cnicos Ambientais para a caracteriza&ccedil;&atilde;o ou n&atilde;o da Insalubridade est&aacute; prevista em v&aacute;rios dispositivos legais, com &ecirc;nfase &agrave; NR 15, item 15.4.1.1, aonde se deduz que o instituto da Insalubridade n&atilde;o pode ser ou deixar de ser caracterizado por mero acaso. H&aacute; de ser lastreado por Laudo T&eacute;cnico de profissional habilitado, Engenheiro de Seguran&ccedil;a do Trabalho ou M&eacute;dico do Trabalho. Nesse sentido compete &agrave;&nbsp;Essence designar um profissional habilitado, a fim de proceder ao levantamento e elabora&ccedil;&atilde;o do Laudo T&eacute;cnico Ambiental, com &ecirc;nfase &agrave;queles comumente desenvolvidos junto aos riscos f&iacute;sicos (com &ecirc;nfase aos n&iacute;veis de press&atilde;o sonora &ndash; ru&iacute;do, &agrave; sobrecarga t&eacute;rmica &ndash; calor, ao frio, a umidade e outros mais) aos riscos qu&iacute;micos e aos riscos biol&oacute;gicos. Ser&atilde;o realizadas medi&ccedil;&otilde;es instrumentalizadas e os correspondentes levantamentos nos postos de trabalho do empregado, visando verificar se as condi&ccedil;&otilde;es atuais de trabalho relacionadas ao agente ambiental em quest&atilde;o, que em fun&ccedil;&atilde;o de sua natureza, concentra&ccedil;&atilde;o ou intensidade e tempo de exposi&ccedil;&atilde;o e que possam causar danos &agrave; sa&uacute;de do trabalhador, est&atilde;o acima dos limites de toler&acirc;ncia estabelecidos na NR 15, quando preconizados os aspectos quantitativos, ou em situa&ccedil;&atilde;o tal que, em decorr&ecirc;ncia da inspe&ccedil;&atilde;o realizada no local de trabalho focada nos aspectos qualitativos, conforme estabelecido na NR 15 e legisla&ccedil;&atilde;o complementar, pela simples constata&ccedil;&atilde;o, possam causar danos &agrave; sa&uacute;de do trabalhador. O laudo contemplar&aacute;, se for o caso, a condi&ccedil;&otilde;es de insalubridade que possam assegurar ao trabalhador o respectivo adicional, bem como a caracteriza&ccedil;&atilde;o de sua gradua&ccedil;&atilde;o. Paralelamente e sob a &oacute;tica da legisla&ccedil;&atilde;o previdenci&aacute;ria, contemplar&aacute;, se for o caso, a condi&ccedil;&atilde;o de nocividade que possa remeter ao recolhimento da al&iacute;quota adicional do SAT, em percentual que oscilar&aacute; conforme o agente nocivo detectado e seu correspondente tempo previsto para a aposentadoria especial. Contemplara, ainda e quando cab&iacute;vel, as formas de elimina&ccedil;&atilde;o ou neutraliza&ccedil;&atilde;o da insalubridade e seus reflexos no &acirc;mbito da nocividade.</p>\r\n\r\n<h3>Laudo T&eacute;cnico de Ergonomia</h3>\r\n\r\n<p>O objetivo deste Laudo &eacute; o de estabelecer par&acirc;metros que permitam a adapta&ccedil;&atilde;o das condi&ccedil;&otilde;es de trabalho &agrave;s caracter&iacute;sticas psicofisiol&oacute;gicas dos trabalhadores, de modo a proporcionar o m&aacute;ximo de conforto, seguran&ccedil;a e desempenho suficientes. Assim, mediante a contrata&ccedil;&atilde;o de tais trabalhos a&nbsp;Essence designar&aacute; equipe t&eacute;cnica que ir&aacute;, &agrave; luz da Norma Regulamentadora 17, trabalhar os aspectos ali estabelecidos, a saber:</p>\r\n\r\n<ul>\r\n	<li>Levantamento e Transporte e Descarga Individual de Materiais;</li>\r\n	<li>Mobili&aacute;rio do Posto de Trabalho;</li>\r\n	<li>Equipamentos do Posto de Trabalho;</li>\r\n	<li>Condi&ccedil;&otilde;es Ambientais de Trabalho e</li>\r\n	<li>Organiza&ccedil;&atilde;o do Trabalho, propondo as a&ccedil;&otilde;es corretivas&nbsp; cab&iacute;veis a cada uma das fun&ccedil;&otilde;es pesquisadas. Para auxiliar nesse processo, s&atilde;o anexadas fotografias digitais dos colaboradores, atrav&eacute;s das quais muitas das quest&otilde;es levantadas podem ser visualizadas.</li>\r\n</ul>\r\n\r\n<h3>Laudo T&eacute;cnico de Atividades Perigosas - NR 16</h3>\r\n\r\n<p>Consiste, em uma das suas modalidades, basicamente em designar um Engenheiro Eletricista, com especializa&ccedil;&atilde;o em Seguran&ccedil;a do Trabalho a fim de proceder ao levantamento e elabora&ccedil;&atilde;o do Laudo T&eacute;cnico Relativo &agrave; Periculosidade diante a Exposi&ccedil;&atilde;o &agrave; Energia El&eacute;trica, com base no Decreto N&ordm; 93.412, de 14.10.1986 e demais par&acirc;metros legais, por fun&ccedil;&atilde;o e respectivos postos de trabalho, cujos trabalhos englobar&atilde;o:</p>\r\n\r\n<p>Os levantamentos nos postos de trabalho de cada empregado, agrupados por fun&ccedil;&atilde;o, visando verificar se as condi&ccedil;&otilde;es atuais de trabalho enquadram-se ou n&atilde;o nas diretrizes do aludido documento legal.</p>\r\n\r\n<p>A verifica&ccedil;&atilde;o de condi&ccedil;&otilde;es de periculosidade que possam assegurar ao trabalhador o respectivo adicional, bem como a defini&ccedil;&atilde;o das eventuais formas de neutraliza&ccedil;&atilde;o da periculosidade e as conclus&otilde;es e recomenda&ccedil;&otilde;es t&eacute;cnicas quanto &agrave;s eventuais a&ccedil;&otilde;es corretivas aplic&aacute;veis ao ambiente ou atividade perigosa, atrav&eacute;s de cronograma a ser anexado ao Programa de Preven&ccedil;&atilde;o de Riscos Ambientais &ndash; P.P.R.A. &ndash; NR 9, da empresa CONTRATANTE.</p>\r\n\r\n<h3>CIPA-NR 5</h3>\r\n\r\n<p>A CIPA - Comiss&atilde;o Interna de Preven&ccedil;&atilde;o de Acidentes, atualizada pela Portaria n&ordm; 8/99 e retificada em 12.07.1999, hoje vislumbrada mais facilmente, na Norma Regulamentadora 5 - NR 5, &eacute; um dos importantes mecanismos de preven&ccedil;&atilde;o de acidentes e doen&ccedil;as decorrentes do trabalho, com objetivo de tornar compat&iacute;vel o trabalho com a preserva&ccedil;&atilde;o da integridade f&iacute;sica e a sa&uacute;de do trabalhador.</p>\r\n\r\n<p>Dada a relativa complexidade da quest&atilde;o, fortemente atrelada a prazos e procedimentos espec&iacute;ficos, a Essence oferece v&aacute;rias modalidades de contrato nesse sentido, a saber:</p>\r\n\r\n<p>A verifica&ccedil;&atilde;o de condi&ccedil;&otilde;es de periculosidade que possam assegurar ao trabalhador o respectivo adicional, bem como a defini&ccedil;&atilde;o das eventuais formas de neutraliza&ccedil;&atilde;o da periculosidade e as conclus&otilde;es e recomenda&ccedil;&otilde;es t&eacute;cnicas quanto &agrave;s eventuais a&ccedil;&otilde;es corretivas aplic&aacute;veis ao ambiente ou atividade perigosa, atrav&eacute;s de cronograma a ser anexado ao Programa de Preven&ccedil;&atilde;o de Riscos Ambientais &ndash; P.P.R.A. &ndash; NR 9, da empresa CONTRATANTE.</p>\r\n\r\n<p>Implanta&ccedil;&atilde;o, que &eacute; a fase preliminar e realizada dentro dos ritos estabelecidos pela NR5 (5.38 a 5.45). Inicia-se com tr&ecirc;s meses de anteced&ecirc;ncia ao t&eacute;rmino do mandato vigente e termina com a posse da CIPA, fornecendo todas as informa&ccedil;&otilde;es e modelos para os Editais, Inscri&ccedil;&otilde;es, C&eacute;dulas, Listas de Vota&ccedil;&atilde;o, Atas de Elei&ccedil;&atilde;o e Apura&ccedil;&atilde;o, Instala&ccedil;&atilde;o e Posse,bem como documentos a serem enviados ao Sindicato da Categoria e DRT.</p>\r\n\r\n<h3>EPI &ndash; NR6</h3>\r\n\r\n<p>Consiste em designar uma Equipe T&eacute;cnica, a fim de proceder ao levantamento e elabora&ccedil;&atilde;o do Laudo T&eacute;cnico, nos termos da Norma Regulamentadora 6, do Minist&eacute;rio do Trabalho e suas altera&ccedil;&otilde;es, cujos trabalhos englobar&atilde;o:</p>\r\n\r\n<ul>\r\n	<li>A apresenta&ccedil;&atilde;o, com base nos dados coletados &ldquo;in loco&rdquo; e todos os documentos e informa&ccedil;&otilde;es cedidas pela CONTRATANTE, de Laudo T&eacute;cnico de Equipamentos de Prote&ccedil;&atilde;o Individual &ndash; E.P.I.&rsquo;s, contemplando todos os empregados e de acordo com as suas peculiaridades, expostos a riscos decorrentes da fase de implanta&ccedil;&atilde;o de medidas de prote&ccedil;&atilde;o coletiva ou daqueles remanescentes de tais medidas, se j&aacute; tiverem sido implantadas ou ainda para o atendimento de situa&ccedil;&otilde;es de emerg&ecirc;ncia.</li>\r\n	<li>Do Laudo T&eacute;cnico em quest&atilde;o constar&atilde;o, ainda, as especifica&ccedil;&otilde;es dos E.P.I.&rsquo;s, com tipo, modelo, marca, materiais; as recomenda&ccedil;&otilde;es de natureza legal relativas a tais dispositivos e ainda as obriga&ccedil;&otilde;es a que a CONTRATANTE, por seu representante legal e a que todos os seus colaboradores, sujeitos ao uso, se submeter&atilde;o.</li>\r\n	<li>&Eacute; de suma import&acirc;ncia observar que, por vezes, a perfeita defini&ccedil;&atilde;o dos EPIs a serem recomendados s&oacute; poder&aacute; ocorrer mediante a apresenta&ccedil;&atilde;o, por parte da empresa, dos laudos ambientais cab&iacute;veis.</li>\r\n</ul>\r\n\r\n<h3>Laudo T&eacute;cnico Edifica&ccedil;&otilde;es - NR 8</h3>\r\n\r\n<p>Consiste em designar uma Equipe T&eacute;cnica , devidamente habilitada, para elaborar o Laudo T&eacute;cnico de Edifica&ccedil;&otilde;es, nos termos da Norma Regulamentadora 8, do Minist&eacute;rio do Trabalho, visando determinar as condi&ccedil;&otilde;es m&iacute;nimas exig&iacute;veis para garantir a seguran&ccedil;a dos empregados da empresa ou seus terceiros, atrav&eacute;s da avalia&ccedil;&atilde;o, naquilo que couber, de:</p>\r\n\r\n<ul>\r\n	<li>Caracter&iacute;sticas construtivas;</li>\r\n	<li>&Aacute;reas de Circula&ccedil;&atilde;o</li>\r\n	<li>Prote&ccedil;&atilde;o contra intemp&eacute;ries.</li>\r\n</ul>\r\n\r\n<p>A Medicina Inteligente &eacute; aquela que previne, diagnostica, acompanha, trata e ao mesmo tempo, reverte todo o processo em benef&iacute;cio para a empresa.</p>\r\n\r\n<p>O Ambulat&oacute;rio M&eacute;dico &eacute; a pr&oacute;pria Medicina Inteligente. Mais que um simples centro de sa&uacute;de, o Ambulat&oacute;rio M&eacute;dico &eacute; um &ldquo;term&ocirc;metro&rdquo; que identifica os problemas e aponta as melhores solu&ccedil;&otilde;es:</p>\r\n\r\n<ul>\r\n	<li>Acompanhamento da popula&ccedil;&atilde;o: O conhecimento detalhado do perfil da popula&ccedil;&atilde;o representa o ponto de partida para a&ccedil;&otilde;es eficazes.</li>\r\n	<li>Redu&ccedil;&atilde;o da sinistralidade: O Seguro Sa&uacute;de &eacute; uma necessidade para todas as empresas. Prevenir e resolver casos mais simples representam ferramentas importantes para defini&ccedil;&atilde;o dos pr&ecirc;mios e a manuten&ccedil;&atilde;o ou melhoria das condi&ccedil;&otilde;es da ap&oacute;lice.</li>\r\n</ul>\r\n\r\n<h3>Assist&ecirc;ncia T&eacute;cnica em Per&iacute;cias Judiciais</h3>\r\n\r\n<p>Por se tratar de uma prerrogativa de todos os cidad&atilde;os, superadas as tentativas de concilia&ccedil;&atilde;o no &acirc;mbito administrativo, o empregado se reserva o direito de acionar judicialmente a empresa. Algumas dessas reclama&ccedil;&otilde;es trabalhistas podem culminar com a necessidade de Per&iacute;cia Judicial, ocasi&atilde;o na qual o Juiz haveria de nomear um Perito para servi-lo e &agrave;s partes fica assegurado o direito de se fazerem representar por Assistentes T&eacute;cnicos, aos quais restariam as incumb&ecirc;ncias de acompanhar os trabalhos do Perito Judicial e manifestarem quando da ocorr&ecirc;ncia de quaisquer diverg&ecirc;ncias quanto ao posicionamento assumido pelo Perito Judicial ou quando houver necessidade de esclarecimentos de natureza t&eacute;cnica</p>\r\n', '0', -1, '2015-04-13 17:54:30', '2015-04-13 17:54:30'),
(2, 'medicina-do-trabalho', 'Medicina do Trabalho', '<h3>Introdu&ccedil;&atilde;o</h3>\r\n\r\n<p>As empresas precisam estar bem assessoradas para o correto atendimento &agrave; legisla&ccedil;&atilde;o. As constantes mudan&ccedil;as obrigam os empregadores a se adaptar rapidamente sob o risco de ter que assumir significativos passivos trabalhistas.</p>\r\n\r\n<h3>PCMSO &ndash; NR7</h3>\r\n\r\n<p>O Programa de Controle M&eacute;dico de Sa&uacute;de Ocupacional, conhecido como PCMSO est&aacute; alicer&ccedil;ado na NR-7 e &eacute; um programa que especifica procedimentos e condutas a serem adotadas pelas empresas em fun&ccedil;&atilde;o dos riscos aos quais os empregados se exp&otilde;em no ambiente de trabalho. Seu objetivo &eacute; prevenir, detectar precocemente, monitorar e controlar poss&iacute;veis danos &agrave; sa&uacute;de do empregado. Implementar o PCMSO &eacute; importante sobretudo para cumprir a legisla&ccedil;&atilde;o em vigor. Al&eacute;m disso, a empresa estar&aacute; se prevenindo contra poss&iacute;veis conseq&uuml;&ecirc;ncias jur&iacute;dicas decorrentes do aparecimento de doen&ccedil;as ocupacionais, como processos c&iacute;veis, criminais e previdenci&aacute;rios.</p>\r\n\r\n<p>O sistema de administra&ccedil;&atilde;o do PCMSO desenvolvido pela Essence permite a seus clientes n&atilde;o somente o cumprimento da legisla&ccedil;&atilde;o vigente, mas principalmente a preserva&ccedil;&atilde;o da sa&uacute;de para o trabalhador e resguardo do empregador.</p>\r\n\r\n<p>Prev&ecirc; as seguintes etapas:</p>\r\n\r\n<ul>\r\n	<li>Levantamento Ambiental</li>\r\n	<li>Elabora&ccedil;&atilde;o do Programa</li>\r\n	<li>Realiza&ccedil;&atilde;o dos Exames</li>\r\n	<li>Gerenciamento</li>\r\n	<li>Guarda dos documentos</li>\r\n	<li>Digitaliza&ccedil;&atilde;o das imagens dos prontu&aacute;rios m&eacute;dicos e disponibiliza&ccedil;&atilde;o de consultas atrav&eacute;s do Essence online</li>\r\n</ul>\r\n', '0', -1, '2015-04-13 17:54:54', '2015-04-13 17:56:04'),
(3, 'qualidade-de-vida', 'Qualidade de Vida', '<h3>Introdu&ccedil;&atilde;o</h3>\r\n\r\n<p>O tema &ldquo;Qualidade de vida no trabalho&rdquo; vem se tornando cada vez mais uma preocupa&ccedil;&atilde;o para a Administra&ccedil;&atilde;o P&uacute;blica e empresas, devido a liga&ccedil;&atilde;o que existe entre condi&ccedil;&otilde;es adequadas para realiza&ccedil;&atilde;o de um trabalho e produtividade, podendo se destacar por v&aacute;rios itens que formam um conjunto de fatores que interferem no desempenho do funcion&aacute;rios.</p>\r\n\r\n<h3>Gin&aacute;stica Laboral</h3>\r\n\r\n<p>Gin&aacute;stica Laboral &eacute; uma atividade saud&aacute;vel composta de exerc&iacute;cios f&iacute;sicos e educativos de alongamento, respira&ccedil;&atilde;o, reeduca&ccedil;&atilde;o postural, controle e percep&ccedil;&atilde;o corporal, executados no pr&oacute;prio local de trabalho. Para obter resultados significativos, recomenda-se a dura&ccedil;&atilde;o de 10 a 15 minutos di&aacute;rios de exerc&iacute;cios. Funcion&aacute;rios administrativos ou das linhas de produ&ccedil;&atilde;o, com a pr&oacute;pria roupa ou uniforme de trabalho podem praticar a gin&aacute;stica laboral, independente do seu ambiente de trabalho, pois a atividade n&atilde;o requer grandes espa&ccedil;os, n&atilde;o provoca suor nem cansa&ccedil;o f&iacute;sico, j&aacute; que tem curta dura&ccedil;&atilde;o e trabalha mais no alongamento e compensa&ccedil;&atilde;o das estruturas musculares.</p>\r\n\r\n<h3>Promo&ccedil;&atilde;o de Sa&uacute;de, Palestras e Campanhas</h3>\r\n\r\n<p>Amplo levantamento epidemiol&oacute;gico da popula&ccedil;&atilde;o da empresa a partir dos dados obtidos dos exames ocupacionais, possibilitando a personaliza&ccedil;&atilde;o das campanhas de Promo&ccedil;&atilde;o de Sa&uacute;de e Qualidade de Vida. Assim, atuamos com foco nos grupos de risco identificados, diferenciando a a&ccedil;&atilde;o.</p>\r\n\r\n<h3>Quick Massagem na Empresa</h3>\r\n\r\n<p>A Quick Massage &eacute; indicada para quem procura um atendimento r&aacute;pido, principalmente para pessoas que possuem pouco tempo e que sofrem com as conseq&uuml;&ecirc;ncias da agitada vida urbana.</p>\r\n', '0', -1, '2015-04-13 17:55:08', '2015-04-13 17:55:44'),
(4, 'gestao-ambiental', 'Gestão Ambiental', '<h3>Introdu&ccedil;&atilde;o</h3>\r\n\r\n<p>Gest&atilde;o ambiental &eacute; um sistema de administra&ccedil;&atilde;o empresarial que d&aacute; &ecirc;nfase na sustentabilidade. Desta forma, a gest&atilde;o ambiental visa o uso de pr&aacute;ticas e m&eacute;todos administrativos que reduzir ao m&aacute;ximo o impacto ambiental das atividades econ&ocirc;micas nos recursos da natureza.</p>\r\n', '0', -1, '2015-04-13 17:55:22', '2015-04-13 17:55:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$e1iYenEyKjTz4xNyVdlDSe454LUPgqVDPiESaGCVdRCZ/ZfKHXkO2', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
