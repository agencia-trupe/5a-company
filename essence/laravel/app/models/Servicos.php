<?php

class Servicos extends Eloquent
{

    protected $table = 'servicos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'titulo' => 'required',
        'texto'  => 'required',
        'imagem' => 'image'
    ];

    public static $imagem_config = [
        'width'  => 750,
        'height' => null,
        'path'   => 'assets/img/servicos/',
        'upsize' => true
    ];

    public function scopePagina($query, $pagina)
    {
        return $query->where('pagina', '=', $pagina);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
