<?php

class TreinamentoCategoria extends Eloquent
{

    protected $table = 'treinamento_categorias';

    protected $hidden = [];

    protected $fillable = ['titulo'];

    public static $rules = [
        'titulo' => 'required'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function arquivos()
    {
        return $this->hasMany('TreinamentoArquivo', 'categoria_id')->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

}