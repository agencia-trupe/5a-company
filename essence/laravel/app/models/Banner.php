<?php

class Banner extends Eloquent
{

    protected $table = 'banners';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'imagem' => 'required|image',
    ];

    public static $imagem_config = [
        'width'  => 980,
        'height' => 480,
        'path'   => 'assets/img/banners/'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
