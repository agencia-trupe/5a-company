<?php

class Contato extends Eloquent
{

    protected $table = 'contato';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'email'      => 'required|email',
        'telefone'   => 'required',
        'endereco'   => 'required',
        'googlemaps' => 'required'
    ];

}
