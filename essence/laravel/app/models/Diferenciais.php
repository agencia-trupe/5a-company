<?php

class Diferenciais extends Eloquent
{

    protected $table = 'diferenciais';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'imagem' => 'image',
    ];

    public static $imagem_config = [
        'width'  => 750,
        'height' => null,
        'path'   => 'assets/img/diferenciais/',
        'upsize' => true
    ];

}
