<?php

class TreinamentoArquivo extends Eloquent
{

    protected $table = 'treinamento_arquivos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'titulo'  => 'required',
        'arquivo' => 'required|mimes:doc,docx,pdf,txt,rtf,zip|max:50000'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->whereCategoriaId($categoria_id);
    }

}