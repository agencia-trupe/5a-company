<?php

class Chamada extends Eloquent
{

    protected $table = 'chamadas';

    protected $hidden = [];

    protected $guarded = ['id'];

    public static $rules = [
        'imagem' => 'image',
        'link'   => 'required',
        'texto'  => 'required'
    ];

    public static $imagem_config = [
        'width'  => 160,
        'height' => 80,
        'path'   => 'assets/img/chamadas/'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
