    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
            @if($contato->facebook || $contato->twitter)
            <nav id="social">
                @if($contato->facebook)<a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>@endif
                @if($contato->twitter)<a href="{{ $contato->twitter }}" class="twitter" target="_blank">twitter</a>@endif
            </nav>
            @endif
            <form action="http://54.94.157.244/login_essence/login.aspx" method="GET" id="form-sistema-gestao">
                <span>Sistema de Gestão Ocupacional</span>
                <input  type="submit" value="Login">
                <label>
                    <span>Usuário</span>
                    <input type="text" name="nome" id="nome" required>
                </label>
                <label>
                    <span>Senha</span>
                    <input type="password" name="senha" id="senha" required>
                </label>
            </form>
            <nav id="menu">
                <a href="{{ route('home') }}"@if(str_is('home', Route::currentRouteName())) class='active'@endif>home</a>
                <a href="{{ route('institucional') }}"@if(str_is('institucional*', Route::currentRouteName())) class='active'@endif>institucional</a>
                <a href="{{ route('diferenciais') }}"@if(str_is('diferenciais', Route::currentRouteName())) class='active'@endif>rede credenciada</a>
                <a href="{{ route('servicos') }}"@if(str_is('servicos*', Route::currentRouteName())) class='active'@endif>serviços</a>
                <a href="{{ route('contato') }}"@if(str_is('contato', Route::currentRouteName())) class='active'@endif>contato</a>
            </nav>
        </div>
        <div class="nav-extension"></div>
    </header>
