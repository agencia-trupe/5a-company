    <footer>
        <div class="center">
            <div class="col">
                <a href="{{ route('home') }}">Home</a>

                <a href="{{ route('institucional') }}">Institucional</a>
                <ul>
                    <li><a href="{{ route('institucional', 'descritivo') }}">Descritivo</a></li>
                    <li><a href="{{ route('institucional', 'missao') }}">Missão</a></li>
                    <li><a href="{{ route('institucional', 'valores') }}">Valores</a></li>
                    <li><a href="{{ route('institucional', 'origem-do-nome') }}">Origem do nome</a></li>
                </ul>
            </div>

            <div class="col">
                <a href="{{ route('diferenciais') }}">Rede Credenciada</a>

                <a href="{{ route('servicos') }}">Serviços</a>
                <ul>
                    <li><a href="{{ route('servicos') }}">Conheça nossos serviços</a></li>
                </ul>

                <a href="{{ route('treinamento') }}">Treinamento a Distância</a>

                <a href="{{ route('contato') }}">Contato</a>
            </div>

            <div class="copyright">
                <img src="{{ asset('assets/img/layout/logo-footer.png') }}" alt="{{ Config::get('projeto.name') }}">
                <p>© {{ date('Y') }} {{ Config::get('projeto.name') }}<br>Todos os direitos reservados.</p>
                <p><a href="http://www.trupe.net" target="_blank">Criação de sites</a>: <a href="http://www.trupe.net" target="_blank" class="trupe">Trupe Agência Criativa</a></p>
            </div>
        </div>
    </footer>
    <div id="footer-empresas">
        <div class="center">
            <a href="http://www.5acompany.com.br" target="_blank">
                <img src="{{ asset('../assets/img/layout/footer/company.png') }}" alt="5A Company">
            </a>
            <a href="http://www.5aconsultorianet.com.br" target="_blank">
                <img src="{{ asset('../assets/img/layout/footer/consultoria.png') }}" alt="5A Consultoria">
            </a>
            <a href="http://www.globalcsc.com.br" target="_blank">
                <img src="{{ asset('../assets/img/layout/footer/globalcsc.png') }}" alt="GlobalCSC">
            </a>
            {{-- <a href="http://www.essencenet.com.br" target="_blank">
                <img src="{{ asset('../assets/img/layout/footer/essence.png') }}" alt="Essence">
            </a> --}}
            <a href="http://www.5a.com.br" target="_blank">
                <img src="{{ asset('../assets/img/layout/footer/gestao.png') }}" alt="5A Gestão de Talentos">
            </a>
        </div>
    </div>
