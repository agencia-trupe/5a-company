@section('content')

    <main id="institucional" class="center">
        <div class="cabecalho">
            <h2>Institucional</h2>
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalho->institucional) }}" alt="">
        </div>

        <div class="content {{ $pagina->pagina }}">
            <nav>
                @foreach($institucional as $menu)
                <a href="{{ route('institucional', $menu->pagina) }}"@if($pagina->pagina == $menu->pagina) class='active'@endif>{{ $menu->titulo }}</a>
                @endforeach
            </nav>

            <div class="texto">
                <div class="texto1">
                    {{ $pagina->texto }}
                </div>
                @if($pagina->imagem)
                <div class="imagem">
                    <img src="{{ asset('assets/img/institucional/'.$pagina->imagem) }}" alt="">
                </div>
                @endif
            </div>
        </div>
    </main>
@stop
