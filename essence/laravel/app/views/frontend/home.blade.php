@section('content')

    <main id="home" class="center">
        <div class="banners-wrapper">
            @foreach($banners as $banner)
            <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="" class="banner">
            @endforeach
            <h3>{{ $frase->frase }}</h3>
        </div>

        <div class="chamadas">
            @foreach($chamadas as $chamada)
            <a href="{{ $chamada->link }}">
                <p>{{ $chamada->texto }}</p>
                <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" alt="">
            </a>
            @endforeach
        </div>

    </main>
@stop
