@section('content')

    <main id="treinamento" class="center">
        <div class="cabecalho">
            <h2>Treinamento a Distância</h2>
        </div>

        <div class="content">
            @foreach($categorias as $categoria)
            <div class="treinamento-categoria">
                <h3>{{ $categoria->titulo }}</h3>
                <ul>
                @foreach($categoria->arquivos as $arquivo)
                <li><a href="{{ url('assets/treinamento/'.$arquivo->arquivo) }}" target="_blank">{{ $arquivo->titulo }}</a></li>
                @endforeach
                </ul>
            </div>
            @endforeach
        </div>
    </main>
@stop
