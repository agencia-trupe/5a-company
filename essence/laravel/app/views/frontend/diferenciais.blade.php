@section('content')

    <main id="diferenciais" class="center">
        <div class="cabecalho">
            <h2>Rede Credenciada</h2>
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalho->diferenciais) }}" alt="">
        </div>

        <div class="content">
            {{ $diferenciais->texto }}
        </div>
    </main>
@stop
