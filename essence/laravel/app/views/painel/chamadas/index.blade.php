@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            <small>Home /</small> Chamadas
        </h2>
    </legend>

    @if(count($chamadas))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="chamadas">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($chamadas as $chamada)

            <tr class="tr-row" id="id_{{ $chamada->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ url('assets/img/chamadas/'.$chamada->imagem) }}" alt="" style="width:100%;max-width:180px;height:auto;"></td>
                <td>{{ $chamada->texto }}</td>
                <td class="crud-actions">
                        <a href="{{ route('painel.chamadas.edit', $chamada->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @endif

@stop
