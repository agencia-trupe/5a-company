@section('content')

    <legend>
        <h2><small>Home /</small> Editar Banner</h2>
    </legend>

    {{ Form::model($banner, [
        'route' => ['painel.banners.update', $banner->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.banners._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop