<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url() }}">{{ Config::get('projeto.name') }}</a>
        </div>

    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown @if(preg_match('~painel.(frase|banners|chamadas)~', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.frase.index') }}">Frase</a></li>
                    <li><a href="{{ route('painel.banners.index') }}">Banners</a></li>
                    <li><a href="{{ route('painel.chamadas.index') }}">Chamadas</a></li>
                </ul>
            </li>

            <li @if(str_is('painel.cabecalhos*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.cabecalhos.index') }}">Cabeçalhos</a>
            </li>

            <li @if(str_is('painel.institucional*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.institucional.index') }}">Institucional</a>
            </li>

            <li @if(str_is('painel.rede-credenciada*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.rede-credenciada.index') }}">Rede Credenciada</a>
            </li>

            <li @if(str_is('painel.servicos*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.servicos.index') }}">Serviços</a>
            </li>

            <li @if(str_is('painel.treinamento-a-distancia*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.treinamento-a-distancia.index') }}">Treinamento a Distância</a>
            </li>

            <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contato @if($recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
                    <li><a href="{{ route('painel.contato.recebidos.index') }}">Contatos Recebidos @if($recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif</a></li>
                </ul>
            </li>

            <li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.usuarios.index') }}">Usuários</a></li>
                    <li><a href="{{ route('painel.logout') }}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    </div>
</nav>
