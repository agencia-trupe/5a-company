@section('content')

    <legend>
        <h2><small>Treinamento a Distância /</small> Editar Categoria</h2>
    </legend>

    {{ Form::model($categoria, [
        'route' => ['painel.treinamento-a-distancia.update', $categoria->id],
        'method' => 'patch'])
    }}

        @include('painel.treinamento._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop