@section('content')

    <legend>
        <h2><small>{{ $categoria->titulo }} /</small> Editar Arquivo</h2>
    </legend>

    {{ Form::model($arquivo, [
        'route' => ['painel.treinamento-a-distancia.arquivos.update', $categoria->id, $arquivo->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.treinamento.arquivos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop