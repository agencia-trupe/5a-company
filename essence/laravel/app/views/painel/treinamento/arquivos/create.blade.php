@section('content')

    <legend>
        <h2><small>{{ $categoria->titulo }} /</small> Adicionar Arquivo</h2>
    </legend>

    {{ Form::open(['route' => ['painel.treinamento-a-distancia.arquivos.store', $categoria->id], 'files' => true]) }}

        @include('painel.treinamento.arquivos._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop