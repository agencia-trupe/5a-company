@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('arquivo', 'Arquivo <small>(pdf, doc, docx, txt, rtf, zip)</small>')) }}
@if($submitText == 'Alterar' && $arquivo->arquivo)
    <a href="{{ url('assets/treinamento/'.$arquivo->arquivo) }}" target="_blank" style="display:block;margin:10px 0;">{{ $arquivo->arquivo }}</a>
@endif
    {{ Form::file('arquivo', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.treinamento-a-distancia.arquivos.index', $categoria->id) }}" class="btn btn-default btn-voltar">Voltar</a>