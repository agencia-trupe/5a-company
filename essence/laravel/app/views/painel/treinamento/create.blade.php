@section('content')

    <legend>
        <h2><small>Treinamento a Distância /</small> Adicionar Categoria</h2>
    </legend>

    {{ Form::open(['route' => 'painel.treinamento-a-distancia.store']) }}

        @include('painel.treinamento._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop