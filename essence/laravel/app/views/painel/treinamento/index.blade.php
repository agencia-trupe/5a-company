@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Treinamento a Distância
            <a href="{{ route('painel.treinamento-a-distancia.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Categoria</a>
        </h2>
    </legend>

    @if(count($categorias))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="treinamento_categorias">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Categoria</th>
                <th>Arquivos</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($categorias as $categoria)

            <tr class="tr-row" id="id_{{ $categoria->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $categoria->titulo }}</td>
                <td>
                    <a href="{{ route('painel.treinamento-a-distancia.arquivos.index', $categoria->id ) }}" class="btn btn-default btn-sm pull-left">
                        <span class="glyphicon glyphicon-file" style="margin-right:10px;"></span>Gerenciar
                    </a>
                </td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.treinamento-a-distancia.destroy', $categoria->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.treinamento-a-distancia.edit', $categoria->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma categoria cadastrada.</div>
    @endif

@stop