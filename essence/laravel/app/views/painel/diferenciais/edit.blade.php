@section('content')

    <legend>
        <h2>Rede Credenciada</h2>
    </legend>

    {{ Form::model($diferenciais, [
        'route' => ['painel.rede-credenciada.update', $diferenciais->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.diferenciais._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
