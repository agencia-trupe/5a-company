<?php

namespace Painel;

use \Diferenciais, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class DiferenciaisController extends BasePainelController {

    public function index()
    {
        $diferenciais = Diferenciais::first();

        return $this->view('painel.diferenciais.edit', compact('diferenciais'));
    }

    public function update($id)
    {
        $diferenciais = Diferenciais::findOrFail($id);
        $input        = Input::all();
        $rules        = Diferenciais::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', Diferenciais::$imagem_config);
            } else {
                unset($input['imagem']);
            }

            $diferenciais->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.rede-credenciada.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
