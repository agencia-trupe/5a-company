<?php

namespace Painel;

use \TreinamentoCategoria, \View, \Input, \Session, \Redirect, \Validator;

class TreinamentoController extends BasePainelController {

    public function index()
    {
        $categorias = TreinamentoCategoria::ordenados()->get();

        return $this->view('painel.treinamento.index', compact('categorias'));
    }

    public function create()
    {
        return $this->view('painel.treinamento.create');
    }

    public function store()
    {
        $input = Input::all();
        $rules = TreinamentoCategoria::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            TreinamentoCategoria::create($input);
            Session::flash('sucesso', 'Categoria criada com sucesso.');

            return Redirect::route('painel.treinamento-a-distancia.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar categoria.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $categoria = TreinamentoCategoria::findOrFail($id);

        return $this->view('painel.treinamento.edit', compact('categoria'));
    }

    public function update($id)
    {
        $categoria = TreinamentoCategoria::findOrFail($id);
        $input  = Input::all();
        $rules  = TreinamentoCategoria::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $categoria->update($input);
            Session::flash('sucesso', 'Categoria alterada com sucesso.');

            return Redirect::route('painel.treinamento-a-distancia.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar categoria.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            TreinamentoCategoria::destroy($id);
            Session::flash('sucesso', 'Categoria removida com sucesso.');

            return Redirect::route('painel.treinamento-a-distancia.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover categoria.']);

        }
    }

}
