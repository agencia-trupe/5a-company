<?php

namespace Painel;

use \TreinamentoCategoria, \TreinamentoArquivo, \View, \Input, \Session, \Redirect, \Validator;

class TreinamentoArquivosController extends BasePainelController {

    public function index($categoria_id)
    {
        $categoria = TreinamentoCategoria::findOrFail($categoria_id);
        $arquivos  = TreinamentoArquivo::categoria($categoria_id)->ordenados()->get();

        return $this->view('painel.treinamento.arquivos.index', compact('arquivos', 'categoria'));
    }

    public function create($categoria_id)
    {
        $categoria = TreinamentoCategoria::findOrFail($categoria_id);

        return $this->view('painel.treinamento.arquivos.create', compact('categoria'));
    }

    public function store($categoria_id)
    {
        $categoria = TreinamentoCategoria::findOrFail($categoria_id);

        $input = Input::all();
        $rules = TreinamentoArquivo::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['categoria_id'] = $categoria->id;

            if (Input::hasFile('arquivo')) {
                $file       = $input['arquivo'];
                $uploadPath = 'assets/treinamento/';
                $fileName   = date('YmdHis').'_'.preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());

                $file->move($uploadPath, $fileName);
                $input['arquivo'] = $fileName;
            }

            TreinamentoArquivo::create($input);
            Session::flash('sucesso', 'Arquivo criado com sucesso.');

            return Redirect::route('painel.treinamento-a-distancia.arquivos.index', $categoria->id);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar arquivo.'])
                ->withInput();

        }
    }

    public function edit($categoria_id, $arquivo_id)
    {
        $categoria = TreinamentoCategoria::findOrFail($categoria_id);
        $arquivo   = TreinamentoArquivo::findOrFail($arquivo_id);

        return $this->view('painel.treinamento.arquivos.edit', compact('categoria', 'arquivo'));
    }

    public function update($categoria_id, $arquivo_id)
    {
        $categoria = TreinamentoCategoria::findOrFail($categoria_id);
        $arquivo   = TreinamentoArquivo::findOrFail($arquivo_id);
        $input     = Input::all();
        $rules     = TreinamentoCategoria::$rules;

        $rules['arquivo'] = 'mimes:doc,docx,pdf,txt,rtf,zip|max:50000';

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['categoria_id'] = $categoria->id;

            if (Input::hasFile('arquivo')) {
                $file       = $input['arquivo'];
                $uploadPath = 'assets/treinamento/';
                $fileName   = date('YmdHis').'_'.preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());

                $file->move($uploadPath, $fileName);
                $input['arquivo'] = $fileName;
            } else {
                unset($input['arquivo']);
            }

            $arquivo->update($input);
            Session::flash('sucesso', 'Arquivo alterado com sucesso.');

            return Redirect::route('painel.treinamento-a-distancia.arquivos.index', $categoria->id);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar arquivo.'])
                ->withInput();

        }
    }

    public function destroy($categoria_id, $arquivo_id)
    {

        $categoria = TreinamentoCategoria::findOrFail($categoria_id);

        try {

            TreinamentoArquivo::destroy($arquivo_id);
            Session::flash('sucesso', 'Arquivo removido com sucesso.');

            return Redirect::route('painel.treinamento-a-distancia.arquivos.index', $categoria->id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover categoria.']);

        }
    }

}
