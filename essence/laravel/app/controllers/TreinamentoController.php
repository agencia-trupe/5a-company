<?php

use \TreinamentoCategoria;

class TreinamentoController extends BaseController {

	public function index()
	{
        $categorias = TreinamentoCategoria::with('arquivos')->ordenados()->get();

		return $this->view('frontend.treinamento', compact('categorias'));
	}

}
