<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'email'      => 'contato@5a.com.br',
                'telefone'   => '11 2344-4595',
                'endereco'   => '<p>S&atilde;o Paulo</p><p>Rua Arizona, 1426 - 4&ordm; andar</p><p>Brooklin - 04567-003</p>',
                'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.835174729641!2d-46.694737700000005!3d-23.610243499999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cc77c3e12d%3A0x84764471e584ebc!2sR.+Arizona%2C+1426+-+Cidade+Mon%C3%A7%C3%B5es%2C+S%C3%A3o+Paulo+-+SP%2C+04567-003!5e0!3m2!1spt-BR!2sbr!4v1427122411829" width="100%" height="100%" frameborder="0" style="border:0"></iframe>',
                'facebook'   => 'http://www.facebook.com',
                'twitter'    => 'http://www.twitter.com'
            )
        );

        DB::table('contato')->insert($data);
    }

}